Hi and welcome to my exam project in Practical programming and numerical methods.

My study number is : 201508947, which lead to project 6.

Where 6: ODE: a two-step method. 

The layout of this repository is as followed:

The directory exam_generator, holds the c# file for generating the project number, and the PDF file uploaded.

The directory "project", is the exam project.

In there, you'll find a library and program directory. In the library directory is the c# used for calculating the 
two-step method. Furthermore, there's also my own rk45 solver, and your vector.cs file.

Inside the folder "project", will you find the main.cs file. Also, you'll find a plot showing the efficiency of the
two-step method. The two differential equation used is some we've seen previously in the course.

The two-step method used here, is the one from the given litterature.

**Don't wanna be a suck up, but I really enjoyed this course,
even though the math was hard at times (I'm an master student in chemical engineering), 
I found the outcome rather high, and it is definitely something I'll use while doing my master thesis. 

Kind regards, 
Frederick Christensen
