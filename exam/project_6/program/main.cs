using System;
using System.IO;
using System.Linq;
using System.Collections.Generic;
using static System.Console;
using static System.Math;
class main{
	static void Main(){
		
		// Starting off with creating the out put.
		var text = new StreamWriter("out.txt");
		
		/* Now defining the initial problems, and the range */		
		double t0 = 0, yt0 = 0.5, a = 0, b = 10;

		/* The exact solution to this differential equation is : 1/(1+Exp(-i)).
		This is the upperhalf of a sigmoid function. */
		Func<double,vector,vector> g = (t,y) => new vector(y[0] * (1-y[0]),0);

		// Defining the ya vector
		vector ya = new vector(yt0,0);
		
		/*Choosing the start step size, and the later stepsizes */
		double h0 = 0.1;
		double h1 = 0.1;

		/*These lists are used for keeping the calculated values from the two-step method */
			
		var xr = new List<double>();
		var yr = new List<vector>();

		// I also created a list for the error.
		var errs = new List<vector>();

		/* Here i run the two-step method with driver (stepsize controller)  */
		vector result = two.twodriver1(g, t0, ya, h0, h1, a, b, ts:xr, ys:yr, err:errs);  

		/* Create the data output */
		var plot = new StreamWriter("data.out.txt");
		
		for(int i = 0; i < xr.Count; i++){
			plot.Write($"{xr[i]} {yr[i][0]} {errs[i][0]}\n");
			}
				
		/* Now using two-step method on u'' = -u */
		
		Func<double,vector,vector> f = delegate(double x, vector y) {return new vector(y[1], -y[0]);};
		vector ya2 = new vector(0,1);

		var xr2 = new List<double>();
		var yr2 = new List<vector>();
		var errs2 = new List<vector>();

		vector result2 = two.twodriver1(f, t0, ya2, h0, h1, a, b, ts:xr2, ys:yr2, err:errs2);
		
		// Separating the output file, so I can plot after index.
		plot.Write("\n\n");
		
		for(int i = 0; i < xr2.Count; i++)
			plot.Write($"{xr2[i]} {yr2[i][0]} {yr2[i][1]} {errs2[i][0]} {errs2[i][1]}\n");
		
		/* Also writing the exact solutions */
		plot.Write("\n\n");
		for(int i = 0; i < b; i++)
			plot.Write($"{i} {1/(1+Exp(-i))} {Sin(i)} {Cos(i)}\n");
		
		plot.Close();
		
		
		
		text.Write($"So for this exam project, two differential equations is approximated by Two-step method.\n");
		text.Write($"\nThe first one is u' = u * (1-u), with ya = 0.5");
		text.Write($"\nThe second is u''=u, with ya = <0,1>\nThe result for both can be seen in the plot.svg");
		text.Close();
	
			
	}
}
