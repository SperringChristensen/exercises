using System;
using static System.Console;
using static System.Math;
using System.Collections;
using System.Collections.Generic;// This is used for creating lists, where the results/objects acan be obtained
// Creating a public class called two, since it's twostepper methods that will be programmed
public partial class two{						
			public static vector[] twstep2(Func<double,vector,vector> f, 
			double t0, vector y0, double step1, double t, vector y, double step2){
			
			/* This creates the twostep method. The two step method used here, is the one
			in the chapter: ODE */
			
			/* Redefining the step sizes */
			double h0 = step1;
			double h1 = step2;
			
			/* The equation that will be used here is : 
			\bar(x) = yi + y'i * h1 + c * h1^2
			Here the y'i(t,y) is the delta y/ delta x, so the slope. */


			/* Creating the y'i(t,y), and the coefficient c.*/
			vector yiApostrophe = f(t, y);
			vector c = (y0 - y + yiApostrophe * h0) / (Pow(h0,2));
			
			/* Now calculating the yi+1 for normal two-step method*/
			vector yi = y + yiApostrophe * h1 + c * Pow(h1,2);
		
			/* Two-step method with extra evaluation can be used to further increase
			the order of approximation (see chapter ODE). Here the governing equation is:
			\bar\bar(y(x)) = \bar(y(x)) + d * h1 ^2 * h0*/

			/* d can be found from: 
			d" = ft - y'i - 2*c (h1)*/
			vector ft = f(t + h1, yi);
						
			vector d = (ft - yiApostrophe - 2 * c * h1) / (2 * h1 * (h1 + h0) + Pow(h1,2));

			/* Now calculating the yi+1 for extra evaluation.
			This function also works as a corrector */
			vector yii = yi + d * Pow(h1,2) * h0; 
			
			vector err = (yii - yi);
						
			vector[] result = {yii,err};
			
			return result;
			}
			
			
			
			/*Now I will create my own driver for simple two-step method.
			This driver do have som similarities with the one in problem ODE, and yours.
			
			**NOTE!
			I did have some problem with the previous driver I have created (problem ODE), 
			it unfortunately made the step sizes close to the smallest decimal for double (64bit).  */


			public static vector twodriver1(Func<double,vector,vector> f, 
				double t0, vector ya, double step1,  double step2,
				double a, double b, double acc = 1e-3, double eps = 1e-3,
				List<double> ts = null, List<vector> ys = null, List<vector> err = null, int limit = 999){
				
				/*For this driver, I will store the values in list form*/
				vector errstart = new vector("0 0");
				// Filling out lists
				if(ts!=null) ts.Add(t0);
				if(ys!=null) ys.Add(ya);
				if(err!=null) err.Add(errstart);
				/* Redefining steps*/
				double h0 = step1;
				double h1 = step2;
				double hnew; // This is created for the case when the h1 doens't satisfy the tolerance
				
				vector y = ya;
				vector[] step;
				
				int nsteps = 0;
				/* I created my normal ODE driver as a While loop. But in this case I will
				do it as a for loop, due to it being more reliable than my previous while loop */
				
					for(double k = a+h1; k<=b; k+=h1){
						nsteps++; // I create this for storing the number of calculations.

						
						/* This if statement is for the first calculated yi.
						I estimate it with Runge Kutta (see ODE.cs) */
						
						if(k == a+h1) step = rk.rkstep45(f, t0, ya, h0);

						/* After the first step, the driver will use two-step method*/   
						else step = twstep2(f, t0, ya, h0, k, y, h1);
						
						/* To use the previous calculated yi values, I store the
						placement, stepsize, and yi values, which the driver can
						use for the next approximation */
						
						t0 = k;
						h0=h1;
						ya = y;						
						
						/* The result from the two-step method comes as two vectors */
						
						vector yh = step[0];
						y = yh; // Doing this to save the yi for the next run
						vector er = step[1];
						
						/* Now creating the adaptive stepsize controller */
						


						// Setting up the tolerance ratios
						vector tolerance = new vector(er.size);
						for(int i = 0; i < tolerance.size; i++){
							tolerance[i] = Max(acc, Abs(yh[i] * eps) * Sqrt(h1/(b-a)));
							if(er[i] == 0) er[i] = tolerance[i]/4;
							}
						
						double errfactor = tolerance[0]/Abs(er[0]);
						for(int i = 1; i < tolerance.size; i++){
							if(tolerance[i] / Abs(er[i]) < errfactor){
								errfactor = tolerance[i] / Abs(er[i]);
								}
							}
						
						/* Now setting up the stepsize controller.
						The size of the new stepsize is given by:
						hnew = h * errorfactor^power * safety */
						 
						for( int i = 1; i<tolerance.size; i++){
							if(Abs(er[i]) > tolerance[i]){
								Error.Write($"Bas step size = {h1}, at {k}\n");
								hnew = h1 * Pow(errfactor, 0.25)*0.95;
								h1 = hnew;
								}
							}
										
						if(ts!=null) ts.Add(k);
						if(ys!=null) ys.Add(yh);
						if(err!=null) err.Add(er);
						}
				vector nstepf = new vector(nsteps);
				return nstepf; // Returns step
				}	
				
}
