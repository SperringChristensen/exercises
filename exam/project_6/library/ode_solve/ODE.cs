using System;
using static System.Console;
using static System.Math;
using System.Collections;
using System.Collections.Generic;// This is used for creating lists, where the results/objects can be obtained
// Creating a public class called Runge Kutta, since it's Runge Kutta methods that will be used
public partial class rk{
	/* Instead of the usual two-point method, or second order method, I will implement fourth-order method.
	The Runge Kutta constants will be contained in vectors. The vector script can be seen in the library */
	public static vector[] rkstep45(Func<double,vector,vector> F, double t, vector yt, double h){
		/* Func = the right hand side of dy/dt = f(t,y).
		t = current value
		vector yt = initial value
		h = step size. Since this is the implicit runge kutta, the governing equation is:
		yn+1 = yn + h sum bi*ki, ki = f(tn + ci*h, yn + h*sum aij * kj */

		/* From the butcher table (https://en.wikipedia.org/wiki/Runge%E2%80%93Kutta_methods) */
		double c2 = 1.0/4, a21 = 1.0/4;
		double c3 = 3.0/8, a31 = 3.0/32, a32 = 9.0/32;
		double c4 = 12.0/13, a41 = 1932.0/2197, a42 = -7200.0/2197, a43 = 7296.0/2197;
		double c5 = 1.0,   a51 = 439.0/216, a52 = -8.0, a53 = 3680.0/513, a54 = -845.0/4104;
		double c6 = 1.0/2.0, a61 = -8.0/27, a62 = 2.0 , a63 = -3544.0/2565, a64 = 1859.0/4104, a65 = -11.0/40;
				double b1 = 16.0/135, b2 = 0, b3 = 6656.0/12825, b4 = 28561.0/56430, b5 = -9.0/50, b6 = 2.0/55;
				double b1s = 25.0/216, b2s = 0, b3s = 1408.0/2565, b4s = 2197.0/4104, b5s = -1.0/5, b6s = 0;

		vector k1 = F(t,yt); 
		
		vector k2 = F(t + c2 * h, yt + h * (a21 * k1)); 
		
		vector k3 = F(t + c3 * h, yt + h * (a31*k1 + a32*k2));
		
		vector k4 = F(t + c4 * h, yt + h * (a41*k1 + a42*k2 + a43*k3));
		
		vector k5 = F(t + c5 * h, yt + h * (a51*k1 + a52*k2 + a53*k3 + a54*k4));
		
		vector k6 = F(t + c6 * h, yt + h * (a61*k1 + a62*k2 + a63*k3 + a64*k4 + a65*k5));
		
		//With yh : 
		vector yh = yt + h*(k1*b1+k2*b2+k3*b3+k4*b4+b5*k5+b6*k6);
		
		//With err:
		vector err = h * ((b1-b1s)*k1 + (b2-b2s)*k2 + (b3-b3s)*k3 +(b4-b4s)*k4 + (b5-b5s)*k5 + (b6-b6s)*k6);

		vector[] result = {yh,err};
		return result;
		}
		



	public static vector rk45(Func<double,vector,vector> F, 
		double a, vector yt, double b, double h, List<double> ts=null, List<vector> ys = null, 
			double acc=1e-3, double eps=1e-3, int limit=999){
			return driver(rkstep45,F,a,yt,b,h,acc,eps,ts,ys);
			}
		
		
		// This is my own driver, just with a "little" inspiration from your script. 
		// I just kept it here, incase that I wanted to compare the two methods. 
		public static vector driver(Func<Func<double,vector,vector>,double,vector,double,vector[]> stepper,
			Func<double,vector,vector> f,
			double a, vector yt, double b, double h, //step size
			double acc, double eps, 
			List<double> ts = null, List<vector> ys = null, int limit=999){
			// setting starting point
			double t = a;
			
			// Filling out the lists
			if(ts!=null) ts.Add(t);
			if(ys!=null) ys.Add(yt);
			vector[] step;
			int nsteps = 0;
			
			vector nstepf = new vector(0);
			//choosing to do it as a while loop
			while(t<=b){
				nsteps++;
				// this had to bee inserted after the roots  B assignment
				if(t>=b) {return yt; }
				if(nsteps>limit) {Error.Write($"driver nsteps>{limit}\n");
					return yt;}
				
				
				if(t+h>b) h=b-t;
				// defining the stepper
				step = stepper(f,a,yt,h);
				
				// making the yi+1 and error vectors.
				vector yh = step[0];
				vector er = step[1];			
				
				// creating the local tolerance for-loop
				vector tol = new vector(er.size);
				for(int i=0; i<tol.size; i++){
					// deltayi = |yi|*eps => estimate of integration error at i
					// tua i = deltayi * sqrt(h/(b-a))
					tol[i]=Max(acc,Abs(yh[i])*eps)*Sqrt(h/(b-a));
					if(er[i]==0)er[i]=tol[i]/4;
					}
				
				// setting up the tolerance ratios
				double errfactor = tol[0]/Abs(er[0]);
				for(int i=1;i<tol.size;i++){
					if(tol[i]/Abs(er[i])<errfactor){
						errfactor=tol[i]/Abs(er[i]);
						}
					}
				
				// Now setting up what to do, if error is lower than tolerance:
				bool factorAccepted = true;
				for(int i=0;i<tol.size;i++){
					if(Abs(er[i])>tol[i]) factorAccepted = false;
				}

				// creating hi+1 = hi * (taui/ei)^power * safety
				double power = 0.25, safety = 0.95;
				double hi1 = h * Pow(errfactor,power)*safety;
				
				if(factorAccepted == true){
					t+=h; yt=yh; h=hi1; 
					if(ts!=null) ts.Add(t);
					if(ys!=null) ys.Add(yt);
					}
				else if(factorAccepted == false){
					// Had to put in error if bad step after assignment roots B.....
					Error.WriteLine($"bad step: x={a}");
					h=hi1;
				}
				nstepf = new vector(nsteps);
				}return nstepf; //return nsteps; return 
		}
	} 
