using System;
public partial class grams{
	public readonly matrix R,Q;
	public grams(matrix A){
		//Q is a copy of matrix A, and R is a matrix mxm
		//The copy operator is already a part of the matrix.cs script from Dimitri
		//the norm function exists in the basic programming
		//this programming is primarily from the literature provided
		Q = A.copy(); int m=Q.size2; R = new matrix(m,m);
		for(int i=0;i<m;i++){
			R[i,i]=Q[i].norm();
			Q[i]/=R[i,i];
			for(int j=i+1; j<m; j++){
				// Q[i] has to be in vector form. % is the modulo operation
				R[i,j] = Q[i]%Q[j];
				Q[j]-=Q[i]*R[i,j];
				}
			}
	}
	public static vector solver(matrix QT, matrix R, vector b){
		vector x = QT*b;
		backward(R, b, x);
		return x;
		}
	
	//updated inverse
	// Inspiration from : The heston Model and its extensions - Book.
	public static matrix inverse(matrix A){
			var QR = new grams(A);
			var R = QR.R;
			var Q = QR.Q;
			var QT = Q.T;
			int m = R.size2;
			matrix Rinv = new matrix(m,m); //This is R inverse
			
			for(int j = m-1; j>= 0;j--){
				Rinv[j,j] = 1.0/R[j,j];
				for(int i=j-1; i>=0;i--){
				for(int k=i+1; k<=j;k++){
					Rinv[i,j] -= 1.0 / R[i,i] * R[i,k] * Rinv[k,j];
				}
				}
				}	
			var C = A.copy();
			C = Rinv * QT;
			
			return C;
		}			


			
			
			

	public static void backward(matrix T, vector b, vector x){
		int i,j;
		x[x.size-1] = x[x.size-1]/T[x.size-1, x.size-1];
		for(i=x.size-2;i>=0;i--){
		//	x[i] = b[i];
				for (j=i+1; j<x.size; j++){
					x[i] -= T[i, j]*x[j];
				}
			x[i]=x[i]/T[i, i];
			}
		}
	public static matrix qr_qs_inverse3x3(matrix Q, matrix R){
			var cofactor = R.copy(); var Rinv = R.copy(); var Ainv = R.copy(); 
			matrix RT = R.T;
			double Rdet = R.Determinant3x3(R);
			cofactor = ADJ(RT);
			Rinv = 1/Rdet*cofactor;
			Ainv = Rinv * Q.T;
			return Ainv;
		}
	public static matrix R2x2inv(matrix R){
			var cofactor = R.copy(); var Rinv = R.copy();
			double Rdet = R.Determinant2x2(R);
			cofactor = ADJ2x2(R);
			Rinv = 1/Rdet*cofactor;
			return Rinv;
		}
	// the adjoint matrix for a 3x3
	public static matrix ADJ(matrix C){
			var D = new matrix(3,3);
			D[0,0] = (C[1,1]*C[2,2] - C[1,2]*C[2,1]); 
			D[0,1] = -(C[1,0]*C[2,2] - C[1,2] * C[2,0]); 
			D[0,2] = (C[1,0]*C[2,1] - C[1,1]*C[2,0]);
			D[1,0] = -(C[0,1]*C[2,2]-C[2,1]*C[0,2]);
			D[1,1] = (C[0,0] * C[2,2] - C[0,2]*C[2,0]);
			D[1,2] = -(C[0,0]*C[2,1]-C[0,1]*C[2,0]);
			D[2,0] = (C[0,1]*C[1,2] - C[0,2]*C[1,1]); 
			D[2,1] = -(C[0,0]*C[1,2]-C[0,2]*C[1,0]);
			D[2,2] = (C[0,0]*C[1,1]-C[0,1]*C[1,0]); 

			//C[0,0] = C[0,0] * 1; C[0,1] = C[0,1] * (-1); C[0,2] = C[0,2]*1;
			//C[1,0] = C[1,0] * (-1); C[1,1] = C[1,1] * 1; C[1,2] = C[1,2] * (-1);
			//C[2,0] = C[2,0] * 1; C[2,1] = C[2,1] *(-1); C[2,2] = C[2,2]*1;
			return D;	
	}
	// the adjoint matrix for a 2x2
	public static matrix ADJ2x2(matrix C){
			var D = new matrix(2,2);
			D[0,0] = C[1,1];
			D[0,1] = -(C[0,1]);
			D[1,0] = -(C[1,0]);
			D[1,1] = C[0,0];
			return D;
	}

	
}

		
		
