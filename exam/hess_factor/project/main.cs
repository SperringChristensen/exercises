using System;
using System.IO;
using System.Diagnostics;
using static System.Console;
using static System.Math;
class main{
	static void Main(){
		int n = 4, random = 5, range = 10; var A = new matrix(n,n);
		
		WriteLine("Project : 3, Hessenberg Factorization of a Real Square Matrix using Jacobi Transformation\n\n");
		
		WriteLine("Starting off with creating the random matrix A\n");
			
		// This creates a randommatrix. You can see the script for this in /library/matrix/matrix.cs
		A = A.randommatrix(n, random, range);
				
		A.print("A matrix = ");
		
		WriteLine("\nFrom this matrix A, the Hessenberg matrix is found by Jacobi (Givens) rotation");
		
		// This can be seen in the jacobi_t.cs
		var H = jacobi.Jmatrix(A);
		H.print("Hessenberg matrix = ");
		
		WriteLine("\nThis is the upper Hessenberg matrix. The lower Hessenberg is found by transposing the matrix:");
		var LH = H.T;
		LH.print("Lower Hessenberg matrix = ");
		WriteLine("**Note that the extremely low values is equal to zero.I've just chosen not to round them down");
		WriteLine("too zero since some Hessenberg factorization results in low values");
		
		WriteLine("\nThrough LU decomposition (see the library for my LU decomposition script)");
		WriteLine("the determinant of the Hessenberg matrix is found");
		
		// Now the LU decomposition can be seen in the library
		double detH = LU.LUdet(H);
		WriteLine($"\nDeterminant(H) = {detH}");
			
		
		// This saves the generated datapoints in an separate .txt file
		var plot = new StreamWriter("data.out.txt");
		// This is used to measure the time
		Stopwatch stopwatch = new Stopwatch();
		Stopwatch stopwatch1 = new Stopwatch();
		
		// Size of the matrix
		int nmatrix = 350;
		for(int i = 2; i<nmatrix; i++){
		matrix B = new matrix(i,i);
		
		B = B.randommatrix(i, 2, 10);
		
		stopwatch.Start();		
		var HF = jacobi.Jmatrix(B);
		stopwatch.Stop();
		
		stopwatch1.Start();
		var QR = new grams(B);
		stopwatch1.Stop();
		
		plot.Write($"{i} {stopwatch.Elapsed.Milliseconds} {stopwatch1.Elapsed.Milliseconds}\n");
		
		stopwatch.Reset(); stopwatch1.Reset();
		}
		
		plot.Close();
		
		WriteLine("\n");
		WriteLine("In this case, the QR factorization is actually faster than my Hessenberg factorization.\n");
		WriteLine("This shouldn't be the case, since the Givens' method (jacobi) requires O(n2) flops\n");
		WriteLine("Rather tha O(n3) for a dense QR factorization.\n\nThe comparison can be seen in the plot.svg");
		
		
		}
	}

