using System;
using static System.Math;
public partial class LU{
	//public readonly matrix lower,upper;
	public static matrix[] luDecomp(matrix A){
	// Here the matrix is decompositioned into upper and lower
	int n = A.size1;
	// Creating the triangles
	var lower = new matrix(n,n);
	var upper = new matrix(n,n);
	
	
	
	for(int i = 0; i < n; i++){
		//Creating upper triangular
		for(int k = i; k<n ; k++){
			double sum = 0; // Sum of L(i,j) * U(j,k)
			for(int j = 0; j < i; j++){
				sum += (lower[i,j] * upper[j,k]);
				}
				//Evaluating U(i,k)
			upper[i,k] = A[i,k] - sum;
		}
		
		// Lower triangular
		for(int k = i; k < n; k++){
			if(i==k) lower[i,i] = 1; //diagonal as 1
			else{
				//Summation of L(k,j) * U(j,i)
				double sum = 0;
				for(int j = 0; j<i; j++){
					sum += (lower[k,j] * upper[j,i]);
					}
				
				// Evaluating L(k,i)
				lower[k,i] = (A[k,i] - sum) / upper[i,i];
			}
		}
		}
		matrix[] result = {lower,upper};
		return result;
	}
	public static double LUdet(matrix A){
		int n = A.size1;
		matrix[] LU = luDecomp(A);
		var L = LU[0];
		var U = LU[1];
				
		//var L = LU.L;
		//var U = LU.U;
		
		double detL = 1, detU=1;
		
		var eL = new vector(n);
		var eU = new vector(n);
		for(int i = 0; i<n; i++){
			eL[i] = L[i,i];
			eU[i] = U[i,i];
			}
		for(int i = 0; i<n; i++){
			detL *= eL[i];
			detU *= eU[i];
		}
		double detA = detL * detU;
		return detA;
		}
}
