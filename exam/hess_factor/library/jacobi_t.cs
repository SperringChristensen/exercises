using System;
using static System.Math;
public partial class jacobi{
	/* So for this project, the Hessenberg factorization by jacobi transformation is programmed.
	First off, i start by creating the rotation (Givens rotation). Here s = sin(phi), and c = cos(phi).
	The reason why I choose too do it this way, is due to its simplicity. 
	*/
	
	/* I should have created the Givens matrix, but for keeping it simple, and making sure that it works,
	I choose to create the rotation for each of the p and q. 
	For litterature, I've used https://en.wikipedia.org/wiki/Givens_rotation*/	
	public static double[] givens(double x, double y){
		double c, s, h;
		if(y == 0){
			c=1;
			s=0;
		}
		else if(Abs(y) >= Abs(x)){
			h = -x/y;
			s = 1.0/Sqrt(1+h*h);
			c = s*h;
			}
		else{
			h = -y/x;
			c = 1.0/Sqrt(1+h*h);
			s = c*h;
			}
		double[] result = {c, s}; /* This returns two values, of which result[0] = c, and result[1] = s*/
		return result;
		}
	
	/* Reduction to Hessenberg form.
	Instead of transforming it into a lower Hessenberg form, I  transform it into a upper Hessenberg form.
	In order to remove the elements a31, a41, ..., an1, and a42 ...., an4, the Jacobi rotation (givens) in row p and column q:
	p = 2, q = 3,4,...,n.
	The rotation angle, c and s, is chosen so that a_q1^new = 0, thereby zeroing the elements a_q,p-1 ^ new, q = p+1,...,n
	This reduces the matrix to the upper Hessenberg form.
	
	But this could also be implemented to transform it into a lower hessenberg. 
	*/
	
	public static matrix Jmatrix(matrix A){
	int n=A.size1, i, p, q; double c, s;
	var V = new matrix(n,n);
	V = V.identity(n);
	
	/* Here V becomes the orthogonal transformation matrix (Jacobi rotation orthogonal transformation). From this, H = V' * A * V. */
	
	/* For each rotation, two rows & columns are transformed */
	
	var h = new vector(n);
	for(p = 1; p<n-1; p++){
		for(q=p+1;q<n;q++){
			double[] res = givens(A[p,p-1],A[q,p-1]); // Finding the Givens to the current step. Note that rotate A(q,p-1) = 0
			c = res[0];
			s = res[1];
			if(s!=0){
			/* Now this can be done more efficient than this. 
			But due to being in a bit of a hurry, I'll just leave it like this.
			
			Now, for each row in the vector h, the current rotation is stored.
			
			The rest of these can be found in the eigen chapter. */		
			
				for(i = 0; i<n; i++){
					h[i] = A[p,i]*c-A[q,i]*s;
					}
				
				for(i = 0; i<n; i++){
					A[q,i] = A[p,i] * s + A[q,i] * c;
					}
				
				for(i = 0; i<n; i++)
					A[p,i] = h[i];
				
				for(i = 0; i<n; i++){
					h[i] = A[i,p] * c - A[i,q]*s;
					}
				for(i = 0; i<n; i++)
					A[i,q] = A[i,p] * s + A[i,q]*c;
				for(i = 0; i<n; i++)
					A[i,p] = h[i];
				for(i = 0; i<n; i++)
					h[i] = V[i,p] * c - V[i,q] * s;
				
				/* Now creating the orthogonal matrix */			
				for(i = 0; i<n; i++)
					V[i,q] = V[i,p] * s + V[i,q] * c;
				for(i = 0; i<n; i++)
					V[i,p] = h[i];
				}
							
			}
		}
	var H = new matrix(n,n);
	H = A.copy();
	return H;
	}
	
		
}
		




			
