Project : 3, Hessenberg Factorization by Jacobi rotation.

Hi again. 

So this time, I've hopefully done the right project.

In the project folder you'll find my out.txt, where I show the factorization, and give it a few comments.
In the plot.svg you'll see the comparison between the Hessenberg factorization and QR factorization.

In the library, you'll see the jacobi_t.cs, which is the .cs file which contains the code for building the Hessenberg factorization.

In the library/matrix directory, you'll find the matrix builder, vector builder, QR factorization, and my newly created LU decomposition,
for calculating the determinant for the Hessenberg matrix.

Most of the comment about the Jacobi rotation and the Hessenberg matrix is in the jacobi_t.cs

- Frederick

