using System;
using static System.Console;
using static System.Math;
static class main{
	static void Main(){
		// n = rows, m = columns
		int n=3; int m=3;
		var A = new matrix(n,m);
		var rand = new Random(2);
		for(int i=0; i<n;i++)
			for(int j=0;j<m;j++)
				A[i,j] = rand.Next(0,10);
		WriteLine("Exercise B \n\n");
		WriteLine("random square matrix\n");
		A.print("A =");
		var QR = new grams(A);
		var Q = QR.Q;
		var R = QR.R;
		WriteLine("A into QR:");
		Q.print("Q =");
		R.print("R =");
		WriteLine("\n\n");
		WriteLine("Now finding the inverse A^-1");
		var Ainv = grams.qr_qs_inverse3x3(Q,R);
		Ainv.print();		
		var AB = A*Ainv;
		WriteLine("Now checking if A*A^1=I\n");
		AB.print("A*A^-1=");
		WriteLine("This shows that A*A^1=I");
		


		// All of this code can be ignored !!!

		// WriteLine("This is done by A^-1=R^-1*Q^T");
		// Creating new matrices for storing values
		// var Ainv = new matrix(n,m); var Rinv = new matrix(n,m); var cofactor = new matrix(n,m);
		// this one creates the cofactor matrix
		// cofactor = grams.ADJ(R.T);
		// finding the determinant. Can be found in matrix.cs
		// double Rdet = R.Determinant3x3(R);
		
		// Rinv = 1/Rdet*cofactor;
		// Ainv = Rinv * Q.T;
		// Ainv.print("A^1=");
		// var AB = A*Ainv;
		// WriteLine("Now checking if A*A^1=I\n");
		//AB.print("A*A^-1=");
		//WriteLine("This shows that A*A^1=I");
		//WriteLine("\n *** The small decimals can be ignored. But could also be removed by a for loop");

		

		}
}
