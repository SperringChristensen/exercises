using System;
public partial class grams{
	public readonly matrix R,Q;
	public grams(matrix A){
		//Q is a copy of matrix A, and R is a matrix mxm
		//The copy operator is already a part of the matrix.cs script from Dimitri
		//the norm function exists in the basic programming
		//this programming is primarily from the literature provided
		Q = A.copy(); int m=Q.size2; R = new matrix(m,m);
		for(int i=0;i<m;i++){
			R[i,i]=Q[i].norm();
			Q[i]/=R[i,i];
			for(int j=i+1; j<m; j++){
				// Q[i] has to be in vector form. I've found a operator in the matrix.cs
				// % which does this. 
				R[i,j] = Q[i]%Q[j];
				Q[j]-=Q[i]*R[i,j];
				}
			}
	}
	public static vector solver(matrix QT, matrix R, vector b){
		vector x = QT*b;
		backward(R, b, x);
		return x;
		}


	public static void backward(matrix T, vector b, vector x){
		int i,j;
		x[x.size-1] = x[x.size-1]/T[x.size-1, x.size-1];
		for(i=x.size-2;i>=0;i--){
		//	x[i] = b[i];
				for (j=i+1; j<x.size; j++){
					x[i] -= T[i, j]*x[j];
				}
			x[i]=x[i]/T[i, i];
			}
		}
	public static matrix qr_qs_inverse3x3(matrix Q, matrix R){
			var cofactor = R.copy(); var Rinv = R.copy(); var Ainv = R.copy(); 
			matrix RT = R.T;
			double Rdet = R.Determinant3x3(R);
			cofactor = ADJ(RT);
			Rinv = 1/Rdet*cofactor;
			Ainv = Rinv * Q.T;
			return Ainv;
		}



	public static matrix ADJ(matrix C){
			var D = new matrix(3,3);
			D[0,0] = (C[1,1]*C[2,2] - C[1,2]*C[2,1]); 
			D[0,1] = -(C[1,0]*C[2,2] - C[1,2] * C[2,0]); 
			D[0,2] = (C[1,0]*C[2,1] - C[1,1]*C[2,0]);
			D[1,0] = -(C[0,1]*C[2,2]-C[2,1]*C[0,2]);
			D[1,1] = (C[0,0] * C[2,2] - C[0,2]*C[2,0]);
			D[1,2] = -(C[0,0]*C[2,1]-C[0,1]*C[2,0]);
			D[2,0] = (C[0,1]*C[1,2] - C[0,2]*C[1,1]); 
			D[2,1] = -(C[0,0]*C[1,2]-C[0,2]*C[1,0]);
			D[2,2] = (C[0,0]*C[1,1]-C[0,1]*C[1,0]); 

			//C[0,0] = C[0,0] * 1; C[0,1] = C[0,1] * (-1); C[0,2] = C[0,2]*1;
			//C[1,0] = C[1,0] * (-1); C[1,1] = C[1,1] * 1; C[1,2] = C[1,2] * (-1);
			//C[2,0] = C[2,0] * 1; C[2,1] = C[2,1] *(-1); C[2,2] = C[2,2]*1;
			return D;	
	}
}

		
		
