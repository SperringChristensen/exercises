using System;
using static System.Console;
using static System.Math;
static class main{
	static void Main(){
		int n=3; int m=3;
		var A = new matrix(n,m);
		var rand = new Random(1);
		for(int i=0; i<n;i++){
		for(int j=0; j<m;j++){
			A[i,j] = rand.Next(0,10);
		}
		}
		// Generating the random vector
		var b = new vector(n);
		b[0] = rand.Next(0,10); b[1] = rand.Next(0,10); b[2] = rand.Next(0,10);
		//b.print();
		WriteLine("Excercise 2. \n\n");
		WriteLine("Random square matrix\n");
		A.print("A=");
		b.print("vector b =");
		//WriteLine(A.Determinant3x3);
		WriteLine("factorizing A into QR \n");
		var QR = new grams(A);
		var Q = QR.Q;
		var R = QR.R;
		var QT = Q.T;
		Q.print("Q=");
		R.print("R=");
		vector x = grams.solver(QT, R, b);
		x.print("x=");
		var Ax = A*x;
		Ax.print("A*x=");

		//double det = A.Determinant3x3(A);
		
		}
}
