using System;
using static System.Console;
using static System.Math;
static class main{
	static void Main(){
		int n=5; int m=4;
		var A = new matrix(n, m);
		var rand = new Random(1);
		for(int i=0; i<n; i++){
		for(int j=0; j<m; j++){
			A[i,j] = rand.Next(0, 10);
		}
		}
		WriteLine("Excercise 1. \n\n");
		WriteLine("Random tall matrix\n");
		A.print();
		// now doing the gramschmidt
		var QR = new grams(A);
		var Q = QR.Q;
		var R = QR.R;
		var QTQ = Q.T*Q;
		var QRA = Q*R-A;
		WriteLine("matrix Q \n");
		Q.print();
		WriteLine("matrix R, can be seen as upper triangular:");
		WriteLine("X X X X \n 0 X X X \n 0 0 X X \n 0 0 0 X \n");
		R.print("R=");
		QTQ.print("Q.T*Q =");
		WriteLine("Checking that Q*R-A is a matrix of all zeros");
		QRA.print();
		WriteLine("I would say that it passes\n");
		}
}
