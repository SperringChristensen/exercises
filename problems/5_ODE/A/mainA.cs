using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
using System.IO;
class main{
	static void Main(){
		double ti = 0, yt0 = 0.5;
		Func<double,vector,vector> f =(t,yt)=> new vector(yt[0] * (1-yt[0]),0);
		vector yta = new vector(yt0,0);
		double h = 1; // step size
		vector[] result12 = rk.rkstep12(f,ti,yta,h);
		vector yh12 = result12[0];
		yh12.print("rkstep12=");
		
		vector[] result23 = rk.rkstep23(f,ti,yta,h);
		vector yh23 = result23[0];
		yh23.print("rkstep23 =");		
		
		//Now trying to use the rk12 with stepper
		var Aplot = new StreamWriter("Aplot.txt");
		
		double tf = 10;
		List<double> ts12 = new List<double>();
		List<vector> ys12 = new List<vector>();
		vector y12 = rk.rk12(f, ti, yta, tf,h, ts:ts12,ys:ys12);
		for(int i=0; i<ts12.Count;i++)
			Aplot.Write($"{ts12[i]} {ys12[i][0]}\n");
		
		Aplot.Write("\n\n");
		List<double> ts23 = new List<double>();
		List<vector> ys23 = new List<vector>();
		
		vector y23 = rk.rk23(f, ti, yta, tf,h, ts:ts23,ys:ys23);

		for(int i=0; i<ts23.Count;i++)
			Aplot.Write($"{ts23[i]} {ys23[i][0]}\n");

		// Printing the exact solution
		Aplot.Write("\n\n");
		for(double i = 0; i<tf; i+= 0.1){
			double exact = 1/(1+Exp(-i));
			Aplot.Write($"{i} {exact} \n");	
		}	
		Aplot.Close();
		
		// now solving u''=-u
		// passing the function with delegates
		// where delegate is a type that represents references to methods with a
		// particular parameter list and return type
		// they alow methods to be passed as parameters
		Func<double,vector,vector> F = delegate(double x, vector y) {return new vector(y[1],-y[0]);};
		
		double a = 0;
		// now defining the initial values, that is:
		// u(0)=1 and u''(0)=0
		vector ya = new vector(0,1);
		double b=2.25*PI;
		double dx = 0.1; // new step size

		var xr12=new List<double>();
		var yr12=new List<vector>();

		var xr23=new List<double>();
		var yr23=new List<vector>();
		
		// comparing rk12 and rk23
		vector u12=rk.rk12(F, a, ya, b, dx, ts:xr12, ys:yr12);
		vector u23=rk.rk23(F, a, ya, b, dx, ts:xr23, ys:yr23);		
				
		var Aplot2 = new StreamWriter("Aplot2.txt");
		
		for(int i=0;i<xr12.Count;i++)		
			Aplot2.Write($"{xr12[i]} {yr12[i][0]} {yr12[i][1]} \n");
		Aplot2.Write("\n\n");
		for(int i=0;i<xr23.Count;i++)
			Aplot2.Write($"{xr23[i]} {yr23[i][0]} {yr23[i][1]} \n");
		
		// now doing the exact solution
		Aplot2.Write("\n\n");
		for(double k=0; k<b; k+=1){
			double exact1 = Sin(k);
			double exact2 = Cos(k);
			Aplot2.Write($"{k} {exact1} {exact2} \n");
		}
		Aplot2.Close();
		
		
		
	}
}
