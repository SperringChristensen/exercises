using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
using System.IO;
class main{
	static void Main(){
		// Starting with defining the parameters
		// N = population size, Tr recovery time (assumed to be 14days)
		// Tc = number of people an infected person infects, Tc = infected per day / recovered per day 
		// wiki and towardsdatascience.com/infectious-disease-modelling-beyond ..
		double N = 5.806e6, Tr = 14, Tc = 6;
		
		// for fun variables:
		
		double alpha = 0.014; // death rate
		
		

		Func<double,vector,vector> SIR= delegate(double t, vector y){
			return new vector(-y[1]*y[0]/(N*Tc),y[1]*y[0]/(N*Tc) - y[1]/Tr,y[1]/Tr);
			};

		double a = 0; //day zero
		double b = 360; //60days
		
		//Building initial values
		
		double infected = 50;
		double susceptible = N - infected;
		double removed = 0;
		
		vector ya = new vector(susceptible, infected, removed);
		
		// Step size
		double h = 0.1;
		
		// building the lists
		var ts = new List<double>();
		var ys = new List<vector>();
		
		// integrating

		vector u23 = rk.rk23(SIR, a, ya, b, h, ts:ts, ys:ys);

		// making the plot 

		var Bplot = new StreamWriter("Bplot.txt");
		
		double total =0;
		for(int i=0;i<ts.Count;i++){
			double dead = ys[i][1]*alpha;
			total += dead;
			Bplot.Write($"{ts[i]} {ys[i][0]} {ys[i][1]} {ys[i][2]} {total}  \n");
			}
		Bplot.Close();
		

		
		}
}
