using System;
using static System.Console;
using static System.Math;
class main{
	static void Main(){
		// starting with defining the function
		Func<double,double> f = (x) => Log(x);
		double a = 0, b = 1, result, result1, result2, d = 2/3;
		result = quad.trapintegrate(f,a,b);
		
		WriteLine($"int_0^1 Log(x) = {result}, the real answer is -1. So pretty close");

		Func<double,double> g = (x) => 4*Sqrt(1-Pow(x,2));
		result1 = quad.trapintegrate(g,a,b);
		
		Func<double,double> h = (x) => Sqrt(x);
		result2 = quad.trapintegrate(h,a,b);
		
		WriteLine($"int_0^1 4*sqrt(1-x^2) = {result1}, should be {PI}");
		WriteLine($"int_0^1 Sqrt(x) = {result2}");

	}
}
