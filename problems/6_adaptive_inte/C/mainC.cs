using System;
using static System.Console;
using static System.Math;
class main{
	static void Main(){
		/*Note to self* If limit is finite, the integral converges,
		if the limit is infinite, or does not exist, the integral diverges*/

		// starting with defining the function
		Func<double,double> f = (x) => 1/(Pow(x,2)+1);		

		// This is a convergent integral
		Func<double,double> g = (x) => Pow(x,-2);
		
		// Divergent integral
		Func<double,double> h = (x) => 4*Exp(-2*x);

		double a = double.NegativeInfinity, b = double.PositiveInfinity, acc1 = 1e-6, eps1 = 1e-6;
		
		double ga = 1,ha=0;
		// defining the results
		double fanaly = PI, ganaly = 1, hanaly = 2;		
					
		
		int calls = 0, gcalls = 0, hcalls = 0,qcalls = 0;
		
		Func<double,double> fevals = (x) => {calls++; return f(x);};
		Func<double,double> qevals = (x) => {qcalls++; return f(x);};
		Func<double,double> gevals = (x) => {gcalls++; return g(x);};
		Func<double,double> hevals = (x) => {hcalls++; return h(x);};
		

		double fresult = quad.integrate_inf(fevals,a,b,acc1,eps1);
		double qresult = quado.o8av(qevals,a,b,acc1,eps1);		
		double gresult = quad.integrate_inf(gevals,ga,b,acc1,eps1);
		double hresult = quad.integrate_inf(hevals,ha,b,acc1,eps1);

		WriteLine($"Equation int 1/(x²+1) from -infinity to +infinity)\nAnalytic result = {fanaly}\n");
		WriteLine($"With parameters: acc = {acc1}, and eps = {eps1}");
		WriteLine($"Result = {fresult}");
		WriteLine($"Evaluations = {calls}");
		WriteLine("\nSolution with Dmitri's quado:");
		WriteLine($"Result = {qresult}");
		WriteLine($"Evaluations = {qcalls}");
		
		WriteLine($"\nEquation int_1^+infinity x^-2\nAnalytic result = {ganaly}");
		WriteLine($"Result = {gresult}");
		WriteLine($"Evaluations = {gcalls}");
		
		WriteLine($"\nEquation int_-infinity^0 1/(3-x)\nAnalytic result = {hanaly}\n");
		WriteLine($"Result = {hresult}\n");
		WriteLine($"Evaluations = {hcalls}");
		}
}
