using System;
using static System.Console;
using static System.Math;
class main{
	static void Main(){
		// starting with defining the function
		Func<double,double> f = (x) => 1/Sqrt(x);
		Func<double,double> g = (x) => Log(x)/Sqrt(x);
		Func<double,double> h = (x) => 4*Sqrt(1-Pow(x,2));		

		double fanaly = 2, ganaly = -4, hanaly = PI;

		// defining the paramters:
		double a = 0, b = 1, acc1 = 1e-2, eps1 = 1e-6;
		
		// defining the results
		double ft, fcc, fquado, gt, gcc, gquado, ht, hcc, hquado;
		
		ft = quad.trapintegrate(f,a,b,acc1,eps1);
		fcc = quad.CC(f,a,b,acc1,eps1);
		fquado = quado.o8av(f,a,b,acc1,eps1);		
		
		double tolft = acc1+eps1*Abs(ft);
		double tolfcc = acc1+eps1*Abs(fcc);
		double tolfquado = acc1+eps1*Abs(fquado);
		
		int calls = 0, calls1 = 0, calls2 = 0;
		
		Func<double,double> ftevals = (x) => {calls++; return f(x);};
		Func<double,double> fccevals = (x) => {calls1++; return f(x);};
		Func<double,double> fquadoevals = (x) => {calls2++; return f(x);};
		
		double ft1 = quad.trapintegrate(ftevals,a,b,acc1,eps1);		
		double fcc1 = quad.CC(fccevals,a,b,acc1,eps1);
		double fquado1 = quado.o8av(fquadoevals,a,b,acc1,eps1);
		
		WriteLine($"Equation 1/Sqrt(x)\nAnalytic result = {fanaly}\n");
		WriteLine($"With parameters: acc = {acc1}, and eps = {eps1}");
		WriteLine($"Without transformation = {ft}\nWith Clenshaw-Curtis = {fcc}\nWith Dmitri's o8av = {fquado}\n");
		WriteLine($"Tolerance without transformation = {tolft}\nWith Cleanshaw-Curtis = {tolfcc}\nWith o8av = {tolfquado}\n");
	
		WriteLine($"Evaluation:\nWithout transformation = {calls}\nWith Clenshaw-Curtis = {calls1}\nWith Dmitir's o8av = {calls2}");
		
		WriteLine("\n\n");
		double acc2 = 1e-3;
		
		gt = quad.trapintegrate(g,a,b,acc2,eps1);
		gcc = quad.CC(g,a,b,acc2,eps1);
		gquado = quado.o8av(g,a,b,acc2,eps1);		
		
		double tolgt = acc2+eps1*Abs(gt);
		double tolgcc = acc2+eps1*Abs(gcc);
		double tolgquado = acc2+eps1*Abs(gquado);
		
		int gcalls = 0, gcalls1 = 0, gcalls2 = 0;
		
		Func<double,double> gtevals = (x) => {gcalls++; return g(x);};
		Func<double,double> gccevals = (x) => {gcalls1++; return g(x);};
		Func<double,double> gquadoevals = (x) => {gcalls2++; return g(x);};
		
		double gt1 = quad.trapintegrate(gtevals,a,b,acc2,eps1);		
		double gcc1 = quad.CC(gccevals,a,b,acc2,eps1);
		double gquado1 = quado.o8av(gquadoevals,a,b,acc2,eps1);
		
		WriteLine($"Equation ln(x)/Sqrt(x)\nAnalytic result = {ganaly}\n");
		WriteLine($"With parameters: acc = {acc2}, and eps = {eps1}");
		WriteLine($"Without transformation = {gt}\nWith Clenshaw-Curtis = {gcc}\nWith Dmitri's o8av = {gquado}\n");
		WriteLine($"Tolerance without transformation = {tolgt}\nWith Cleanshaw-Curtis = {tolgcc}\nWith o8av = {tolgquado}\n");
	
		WriteLine($"Evaluation:\nWithout transformation = {gcalls}\nWith Clenshaw-Curtis = {gcalls1}\nWith Dmitir's o8av = {gcalls2}");
		WriteLine("\n\n");
		double acc3 = 1e-4;
		
		ht = quad.trapintegrate(h,a,b,acc3,eps1);
		hcc = quad.CC(h,a,b,acc3,eps1);
		hquado = quado.o8av(h,a,b,acc3,eps1);		
		
		double tolht = acc3+eps1*Abs(ht);
		double tolhcc = acc3+eps1*Abs(hcc);
		double tolhquado = acc3+eps1*Abs(hquado);
		
		int hcalls = 0, hcalls1 = 0, hcalls2 = 0;
		
		Func<double,double> htevals = (x) => {hcalls++; return h(x);};
		Func<double,double> hccevals = (x) => {hcalls1++; return h(x);};
		Func<double,double> hquadoevals = (x) => {hcalls2++; return h(x);};
		
		double ht1 = quad.trapintegrate(htevals,a,b,acc3,eps1);		
		double hcc1 = quad.CC(hccevals,a,b,acc3,eps1);
		double hquado1 = quado.o8av(hquadoevals,a,b,acc3,eps1);
		
		WriteLine($"Equation 4*Sqrt(1-x^2)\nAnalytic result = {hanaly}\n");
		WriteLine($"With parameters: acc = {acc3}, and eps = {eps1}");
		WriteLine($"Without transformation = {ht}\nWith Clenshaw-Curtis = {hcc}\nWith Dmitri's o8av = {hquado}\n");
		WriteLine($"Tolerance without transformation = {tolht}\nWith Cleanshaw-Curtis = {tolhcc}\nWith o8av = {tolhquado}\n");
	
		WriteLine($"Evaluation:\nWithout transformation = {hcalls}\nWith Clenshaw-Curtis = {hcalls1}\nWith Dmitir's o8av = {hcalls2}\n");
		
		WriteLine($"Accuracy :\nWithout transformation = {hanaly/ht*100} %\nWith Clenshaw-Curtis = {hcc/hanaly*100} %\nWith o8av = {hquado/hanaly*100} %");
		
		WriteLine("\nIt is seen that these methods are actually quite accurate. Dmitri's method are the one who requires the least amount of evaluations.");


	}
}
