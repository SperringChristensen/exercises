using System;
using static System.Console;
using static System.Math;
class main{
	static void Main(){
		//starting with building the Hamiltonian matrix
		int n=20;
		double s=1.0/(n+1);
		matrix H = new matrix(n,n);
		for(int i=0;i<n-1;i++){
			matrix.set(H,i,i,-2);
			matrix.set(H,i,i+1,1);
			matrix.set(H,i+1,i,1);
		}
		matrix.set(H,n-1,n-1,-2);
		matrix.scale(H,-1/s/s);
		matrix V = new matrix(n,n);
		vector e = new vector(n);
		// doing the jacobi cyclic
		int sweeps = jacobi.cyclic(H,e,V);
		// checking that the energies are correct:
		for(int k=0;k<n/3;k++){
			double exact = PI*PI*(k+1)*(k+1);
			double calculated = e[k];
			WriteLine($"{k} {calculated} {exact}");
		}
		//Plotting the several lowest eigenfunctions, and comparing
		//with the analytical result
		WriteLine("\n\n");
		//Actually not quite sure if this gives the wavefunction?
		for(int p=0;p<5;p++){
			WriteLine($"{0} {0}");
			for(int i=0;i<n;i++){
				WriteLine($"{(i+1.0)/(n+1)} {matrix.get(V,i,p)}");}
			WriteLine($"{1} {0}\n");
		}
		}
}
