using System;
using static System.Console;
using static System.Math;
class main{
	static void Main(){
		// starting off with designing the random matrix
		int n=3, random=2; var A = new matrix(n,n);
		A = A.randomsym(n,random);
		// this can be found in matrix.cs		


		WriteLine("Exercise A1 \n\nStarting with printing matrix A");
		A.print("A=");
		WriteLine("\n\n");
		// Doing the jacobi sweep
		vector e = new vector(n);
		matrix V = new matrix(n,n);
		int sweeps = jacobi.cyclic(A,e,V);
				
		// Now doing the eigenvalue decomposition
		// A=V*D*V.T
		// finding the diagonal matrix
		WriteLine("The new calculated matrix V:");
		V.print("V=");
		WriteLine("Number of sweeps={0} e[n-1]={1}\n",sweeps,e[n-1]);
		WriteLine("\nThe diagonal matrix D:");
		var VT = V.T;

		// You don't know how much trouble this caused me..
		// I totally forgot about the order of multiplication
		// for matrices... I spend about 2 hours debugging, before
		// I remembered the order of multiplication. 
		var D = VT*A*V;
		D.print("D=");
		WriteLine("with the eigenvalues:");
		e.print("eigenvalues=");

	}
}
