using System;
using static System.Console;
using static System.Math;
using System.Diagnostics;
using System.IO;
class main{
	static void Main(){
		//int n=4, random = 1;
		//matrix A = new matrix(n,n);
		//A = A.randomsym(n,random);
		//Checking that the number of operations
		//scales as O(n3) (doing a sweep)
		
		var B1 = new StreamWriter("B1.txt");
		Stopwatch stopwatch = new Stopwatch();
		
		// Matrix diagonalization times:
		int n = 150;
		for(int i = 2; i<n; i++){
			matrix B = new matrix(i,i);
			B = B.randomsym(i, 2);
			vector e = new vector(i);
			matrix V = new matrix(i,i);
			stopwatch.Start();
			int sweeps = jacobi.cyclic(B,e,V);
			stopwatch.Stop();
			B1.Write($"{i} {stopwatch.Elapsed.Milliseconds}  {sweeps}\n");
			stopwatch.Reset();
			}
			B1.Close();
		WriteLine("B2 can be seen in jacobi.cs");
		Stopwatch stopwatch2 = new Stopwatch();
		Stopwatch stopwatch3 = new Stopwatch();
		Stopwatch b3script = new Stopwatch();
		// doing assignment 3 & 4
		var B3 = new StreamWriter("B3.txt");
		b3script.Start();
		
		for(int i = 0; i<n; i++){
			// I define new matrices and vectors 
			// since the function return a new matrix
			matrix B = new matrix(i,i);
			matrix C = new matrix(i,i);
			
			B = B.randomsym(i,2);
			C = C.randomsym(i,2);
			
			vector e = new vector(i);
			vector e1 = new vector(i);
			
			matrix V = new matrix(i,i);
			matrix V1 = new matrix(i,i);
			
			stopwatch.Start();
			int sweeps = jacobi.cyclic(B,e,V);
			stopwatch.Stop();
			stopwatch2.Start();
			int rotations = jacobi.onerow(C,e1,1,V1);
			stopwatch2.Stop();
			
			B3.Write($"{i} {stopwatch.Elapsed.Milliseconds} {stopwatch2.Elapsed.Milliseconds} {Log10(sweeps)} {Log10(rotations)}\n");
			stopwatch.Reset(); stopwatch2.Reset(); 
			}
		B3.Write("\n\n");
		
		int n2 = 43;
		for(int i=0; i<n2; i++){
			matrix C2 = new matrix(i,i);
			C2 = C2.randomsym(i,2);
			vector e2 = new vector(i);
			matrix V2 = new matrix(i,i);
			stopwatch3.Start();
			int allro = jacobi.onerow(C2,e2,i,V2);
			stopwatch3.Stop();
			B3.Write($"{i} {stopwatch3.Elapsed.Milliseconds} {Log10(allro)}\n");
			stopwatch3.Reset();
			}
			
			B3.Close();
			b3script.Stop();
			WriteLine("B3 and B4 can be seen in the b3 plot.\n ***I don't have a computer fast enough to calculate 40<n atm. I might do these calculations later on my desktop, which is way faster");
			WriteLine($" Time for script {b3script.Elapsed.Seconds}");
			WriteLine($"\n\nProblem B.5 : Finding the largest eigenvalue of matrix:");
			int n3 = 5;
			matrix A = new matrix(n3,n3);
			A = A.randomsym(n3,2);
			A.print("A=");
			vector e3 = new vector(n3);
			matrix V3 = new matrix(n3,n3);
			int sweeps2 = jacobi.cyclic(A,e3,V3);
			WriteLine("Finding the diagonal matrix\n");
			var VT = V3.T;
			var D = VT*A*V3;
			D.print("D=");
			WriteLine($"Number of sweeps = {sweeps2} and largest eigenvalue {e3[n3-1]}");
			vector e4 = new vector(n3);
			matrix V4 = new matrix(n3,n3);
			int rotation2 = jacobi.onerow(A,e4,1,V4);

			WriteLine($"The onerow rotation only gives the lowest eigenvalue {e4[0]}");
			WriteLine("\nBy substituting phi with phi = 0.5 Atan2(-2*apq, app-aqq) the largest eigenvalue is found as :");
			vector e5 = new vector(n3);
			matrix V5 = new matrix(n3,n3);
			int rotation3 = jacobi.onerowlargest(A,e5,1,V5);
			WriteLine($" {e5[0]}");			

		}
}
