using System;
using System.IO;
using static System.Math;
using static System.Console;
class main{
	static void Main(){
		
		string path = "../data/higgs.txt";
		string[] data = read.text(path);
		
		// E is energy
		vector E = new vector(data[0]);
		// signal 
		vector signal = new vector(data[1]);
		vector err = new vector(data[2]);
		
		Func<vector,double> f = (v) =>{
			double m = v[0];
			double gamma = v[1];
			double A = v[2];
			double sum=0;
			for(int i=0;i<E.size;i++){
				sum+=Pow((A*(1/(Pow(E[i]-m,2) + gamma*gamma /4)) - signal[i]),2) /err[i]/err[i];
			}
		return sum;
		};
	
		WriteLine("Fitting the Briet-Wigner function to the Higgs boson data:\n");
		// Now defining starting points:
		WriteLine($"Guessed points:\nMass = 126.5\nGamma = 2.5\nGuessed scale = 9\n");
			
		vector xstart = new vector("126.5 2.5 9");
		vector h = quasi.hessian(f, xstart);
		h.print("Hessian vector = ");
		
		vector[] result = quasi.min(f, xstart, acc:1e-10);
		vector min = result[0];
		vector steps = result[1];
		
		WriteLine($"Steps = {steps.size}");
		WriteLine($"Approximated mass, gamma and scale:");
		WriteLine($"Mass = {min[0]}\nGamma = {min[1]}\nA = {min[2]}");
		
		// Now creating the plot:'
		var Bplot = new StreamWriter("Bplot.txt");
		
		for(double e=E[0]; e<=E[E.size-1]; e+=1e-3){
		
			Bplot.Write($"{e} {min[2] * (1/(Pow(e-min[0],2) + min[1]*min[1] /4))}\n");
		
			}
		Bplot.Close();
		
		var orgd = new StreamWriter("orgdata.txt");
		
		for(int i = 0; i<E.size; i++)
			orgd.Write($"{E[i]} {signal[i]} {err[i]}\n");
		orgd.Close();
	
	}
}
