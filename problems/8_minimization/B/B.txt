Fitting the Briet-Wigner function to the Higgs boson data:

Guessed points:
Mass = 126.5
Gamma = 2.5
Guessed scale = 9

Hessian vector =       4.49      0.537     -0.353 
Steps = 1258
Approximated mass, gamma and scale:
Mass = 125.972176620811
Gamma = 2.08607676206585
A = 9.87497006508196
