using System;
using System.IO;
using static System.Console;
using static System.Math;
class main{
	static void Main(){
		
		WriteLine($"The Rosenbrock's valley functionn\n");		

		Func<vector,double> f = (v) => Pow(1-v[0],2) + 100 * Pow(v[1] - v[0]*v[0],2);
		vector xstart = new vector("1.5 1.5");
		xstart.print("x starting points = ");
		
		vector h = quasi.hessian(f, xstart);
		h.print("hessian vector = ");

		vector[] result = quasi.min(f,xstart);
		vector steps = result[1];
		WriteLine($"Steps = {steps.size}");
		vector min = result[0];
		min.print("min =");
		WriteLine("Exact min = [1,1]\n");		

		WriteLine($"\nHimmelblaus function : f(x,y) = (x^2+y-11)^2 + (x+y^2-7)^2");
		Func<vector,double> g = (v) => Pow(v[0]*v[0]+v[1]-11,2) + Pow(v[0] + v[1]*v[1] - 7,2);
		vector xstart1 = new vector("-1.5 3.5");
		xstart1.print("x starting points = ");

		vector h1 = quasi.hessian(g,xstart1);
		h1.print("hessian vector = ");
		
		vector[] result1 = quasi.min(g,xstart1);
		vector steps1 = result1[1];
		WriteLine($"Steps = {steps1.size}");
		vector min1 = result1[0];
		min1.print("min=");
		WriteLine("Exact min = [-2.81,3.13]");
	}
}
