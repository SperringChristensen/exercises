using System;
using static System.Console;
using static System.Math;
class main{
	static void Main(){
		int n=5;
		double dx=0.001;
		double[] x = new double[n];
		double[] y = new double[n];
		double l = x.Length;
		//making the points
		int i;
		for(i=0;i<l;i++){
			x[i]=2*PI*i/(n-1);
			y[i]=Sin(x[i]);
			WriteLine($"{x[i]} {y[i]}");
		}
		Write("\n\n");
		var cs = new cspline(x,y);
		double z;
		for(z=x[0]; z<20; z+=dx){
			WriteLine($"{z} {cs.eval(z)} {cs.integ(z)} {cs.deriv(z)}");
		}
	}
}
