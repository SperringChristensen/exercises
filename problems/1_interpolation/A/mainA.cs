using System;
using static System.Console;
using static System.Math;
class main{
	static void Main(){
		int n=5; 
		double dx=0.001;
		double[] x = new double[n];
		double[] y = new double[n];
		double l = x.Length;
		for(int i=0; i<l; i++ ){
			x[i]=2*PI*i/(n-1);
			y[i]=Sin(x[i]);
			WriteLine($"{x[i]} {y[i]}");
		}	
		Write("\n\n");	
		for(double z = x[0]; z<10; z += dx){
			double spline = lspline.linterp(x, y, z);
			double inter = lspline.linterpInteg(x, y, z);
			WriteLine($"{z} {spline} {inter}");
			//WriteLine("{0:f16} {1:f16}",z,interp);
		}
	}
}
