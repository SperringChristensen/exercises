using System;
using static System.Console;
using static System.Math;
class main{
	static void Main(){
		int n=5;
		double dx = 0.01;
		double y,yint,yderiv;
		for(double x = 0; x<10; x+=dx){
			y=Sin(x);
			yint=1-Cos(x);
			yderiv=Cos(x);
			WriteLine($"{x} {y} {yint} {yderiv}");
		}
	}
}
