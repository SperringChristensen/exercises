using System;
using static System.Math;
public class qspline{
	double[] x,y,b,c;
	// starting with creating the spline
	public qspline(double[] xr, double[] yr){
		int n = xr.Length;
		x=new double[n];
		y=new double[n];
		for(int i=0;i<n;i++)
			{x[i]=xr[i];y[i]=yr[i];}
		b=new double[n-1];
		c=new double[n-1];
		var deltax = new double[n-1];
		var p = new double[n-1];
		for(int i=0;i<n-1;i++)
			{deltax[i] = x[i+1] - x[i]; p[i] = (y[i+1]-y[i])/deltax[i];}
		c[0]=0;
		for(int i=0;i<n-2;i++)
			{c[i+1] = (p[i+1] - p[i]-c[i]*deltax[i])/deltax[i];}
		c[n-2] /= 2;
		for(int i=n-3;i>=0;i--)
			{c[i] = (p[i+1]-p[i]-c[i+1]*deltax[i+1])/deltax[i];}
		for(int i=0;i<n-1;i++)
			{b[i]=p[i]-c[i]*deltax[i];}
		}
	public double eval(double z){
		int i = binsearch.search(x,z);
		double dx=z-x[i];
		return y[i]+dx*(b[i]+dx*c[i]);
		}
	public double deriv(double z){
		int i = binsearch.search(x,z);
		double dx=z-x[i];
		return b[i] + 2*c[i]*dx;
		}
	public double integ(double z){
		int iz = binsearch.search(x,z);
		double sum=0,dx;
		for(int i=0;i<iz;i++){
			dx=x[i+1]-x[i];
			sum+=1/3*Pow(dx,3)*c[i]+1/2*Pow(dx,2)*b[i]+y[i]*dx;
		}
		dx=z-x[iz];
		sum+=1/3*Pow(dx,3)*c[iz]+1/2*Pow(dx,2)*b[iz]+y[iz]*dx;
		return sum;
	}
}
