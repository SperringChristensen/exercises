using System;
using static System.Console;
using static System.Math;
class main{
	static void Main(){
		// the integral in this one is way more sensitive to the number known numbers
		int n=5;
		double dx=0.01;
		double[] x = new double[n];
		double[] y = new double[n];
		double l = x.Length;
		// making the points
		int i;
		for(i=0; i<l; i++){
			x[i]=2*PI*i/(n-1);
			y[i]=Sin(x[i]);
			WriteLine($"{x[i]} {y[i]}");
			}
			Write("\n\n");
		var qs = new qspline(x,y);
		double z;
		//WriteLine($"{qs}");
		for (z=x[0]; z<10; z += dx){
			WriteLine($"{z} {qs.eval(z)} {qs.integ(z)} {qs.deriv(z)}");
			}
		}
}
