using System;
using static System.Console;
using static System.Math;
static class main{
	static void Main(){
		string path = "../library/data.txt";
		string[] numb  = read.text(path);
		var x = new vector(numb[0]);
		var y = new vector(numb[1]);
		var dy = new vector(y*0.05);
		var logy = new vector(x.size); var logdy = new vector(x.size);
		for(int i=0;i<x.size;i++){
			logy[i] = Log(y[i]);
			logdy[i] = Log(dy[i]);
		}
		
		// function f is the weighted function
		var f = new Func<double,double>[]{(z) => 1, (z) => z};
		var c = new vector(x.size);
		// now fitting the data
		c = fit.ls(x,logy,logdy,f);
		// writing the coefficients
		// and rearrange them
		double lna = c[0]; double lambda = c[1];
		double halflife = Log(2)/(-lambda);
		WriteLine("ln(a) = {0}, and lambda(decay constant) ={1}, with a half life of = {2} d\n the plot can be seen in the plot.svg file",lna,lambda,halflife);
		WriteLine($"The updated half life for thx is 3.66 d, this means that the half life found, is a bit off compared to the modern value.");

		//making the fit.
		// The model is ln(y) = ln(a) - lambda*t, with
		// c[0] = lambda, and c[1] = ln(a)
		
		// Now defining the linear combination, Fe(z)=sum^m_k=1 ck fk(z)

		var txtout = new System.IO.StreamWriter("out.plot.txt");
		Func<double,double> fitting = z =>{
		double fe = 0;
		for(int k=0;k<f.Length;k++){
			fe+=c[k]*f[k](z);
		}
		return fe;
		};

		// Need to return the plot in a seperate file
		for(double z =0; z<22; z+=0.01){
			txtout.Write("{0} {1}\n", z, Exp(fitting(z)));
			}
		txtout.Close();
		

		
		var plotdata = new System.IO.StreamWriter("orgdata.txt");
		for(int i=0;i<x.size;i++){
			plotdata.Write("{0} {1} {2}\n",x[i],y[i],dy[i]);
			}
		plotdata.Close();
		
		// assignment B is also a part of this main
		//printing in folder B
		var assB = new System.IO.StreamWriter("../B/B.txt");
		var cov = new matrix(x.size,f.Length);
		// finding the covariance matrix
		cov = fit.covariance(x,logy,logdy,f);
		assB.Write($"covariance matrix = \n {cov[0,0]} {cov[0,1]}\n {cov[1,0]} {cov[1,1]}\n");
		// Now finding the uncertanities
		// dc = sqrt(sigma)
		double dc = Sqrt(cov[0,0]*cov[1,1]);
		assB.Write("\n\n");
		assB.Write($"The uncertainties of the fitting coefficients: \n a = {Exp(lna)} +- {dc*Exp(lna)} \n lambda = {-lambda} +- {dc*(-lambda)}\n");
		double dlambda = (dc*(-lambda))/(-lambda);
		assB.Write($"The uncertainty for half time :\n t1/2 = {halflife} d +- {halflife*dlambda} d\n");  
		assB.Write("\n Even with the calculated uncertainty, the calculated half time is still a bit off. \n");
		assB.Close();

		// Assignment C

		var assC = new System.IO.StreamWriter("../C/C.txt");
		
		Func<double,double> fit_c0plus = z =>{
		double dfe = 0;	var c1 = c.copy(); double dc0 = dc;
		c1[0] = c1[0] + dc0;
		for(int k=0;k<f.Length;k++){
			dfe+=c1[k]*f[k](z);
		}
		return dfe;
		};
		Func<double,double> fit_c0minus = z =>{
		double dfe = 0;	var c1 = c.copy(); double dc0 = dc;
		c1[0] = c1[0] - dc0;
		for(int k=0;k<f.Length;k++){
			dfe+=c1[k]*f[k](z);
		}
		return dfe;
		};
		Func<double,double> fit_c1plus = z=>{
		double dfe = 0; var c1 = c.copy(); double dc1 = dc*(-lambda);
		c1[1] = c1[1] + dc1;
		for(int k=0;k<f.Length;k++){
			dfe+=c1[k]*f[k](z);
			}
		return dfe;
		};
		Func<double,double> fit_c1minus = z=>{
		double dfe = 0; var c1 = c.copy(); double dc1 = dc*(-lambda);
		c1[1] = c1[1] - dc1;
		for(int k=0;k<f.Length;k++){
			dfe+=c1[k]*f[k](z);
			}
		return dfe;
		};
		for(double z=0;z<22;z+= 0.01){
		assC.Write("{0} {1} {2} {3} {4}\n",z,Exp(fit_c0plus(z)),Exp(fit_c0minus(z)),Exp(fit_c1plus(z)),Exp(fit_c1minus(z)));
		}
		
		assC.Close();
	//	return dfe;
	//	}
		 
	}
}

