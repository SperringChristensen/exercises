using System;
using static System.Console;
using static System.Math;
public partial class fit{
	public static vector ls(vector x, vector y, vector deltay, Func<double,double>[] f){
		// starting off with building matrix A and vector b
		int n = x.size; int m = f.Length;
		var A = new matrix(n,m);
		var b = new vector(n);
		var c = new vector(n);
		for(int i=0;i<n;i++){
			b[i] = y[i]/deltay[i];
			for(int k=0;k<m;k++){
				A[i,k] = f[k](x[i])/deltay[i];
				}
			}
		// QR factorization
		var QR = new grams(A);
		var Q = QR.Q;
		var R = QR.R;
		// Now using the more simple method to calculate the inverse
		// this is due to now having built a proper solver
		// the code for this, is in matrix.cs
		var Rinv = grams.R2x2inv(R);
		c = Rinv*Q.T*b;
		return c;
		}

	// This is for evaluating the matrix A

	public static matrix covariance(vector x, vector y, vector deltay, Func<double,double>[]f){
	int n = x.size; int m = f.Length;
	var A = new matrix(n,m);
	var b = new vector(n);
	for(int i=0;i<n;i++){
		b[i] = y[i]/deltay[i];
		for(int k=0;k<m;k++){
			A[i,k] = f[k](x[i])/deltay[i];
			}
		}
	var QR = new grams(A);
	var Q = QR.Q;
	var R = QR.R;
	var RT = R.T;
	var cov =grams.R2x2inv(R)* grams.R2x2inv(RT);
	// The diogonal entries of the covariance matrix are the variances
	// the other entries is the covariances. 
	return cov;
	}
}
