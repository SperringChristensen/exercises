using System;
using System.IO;
using static System.Math;
using static System.Console;
class main{
	static void Main(){
		int n = 5;
		
		//Activation function
		Func<double,double> f = (x) => x*Exp(-x*x);
		
		//function to fit
		//Func<double,double> g = (x) => Cos(5*x-1)*Exp(-x*x);
		Func<double,double> g = (x) => Cos(x);
		Func<double,double> gd = (x) => -Sin(x);
		Func<double,double> gi = (x) => Sin(x);		

		int nx = 20;
		vector xs = new vector(nx);
		vector ys = new vector(nx);
		
		double a = -5;
		double b = 5;
		double w = 1;
		
		
		var Bplot = new StreamWriter("Bplot.txt");
		
		for(int i = 0; i<nx; i++){
			xs[i] = a+(b-a)*i/(nx-1);
			ys[i] = g(xs[i]);
			Bplot.Write($"{xs[i]} {ys[i]} {gd(xs[i])} {gi(xs[i])} \n");
			}
		
		// now training the function
		// *Do remember to change the steplim. 
		// Steplim has a huge impact upon the accuracy of the minimization
		vector train = nn.train(xs,ys, n, f, a:a,b:b,steplim:9999);
		
		train.print("p new = ");
		
		Bplot.Write("\n\n");
		
		// building the network
		;// = new vector(2*(1/64));
		
		double dx = b/64;
		vector xr = new vector(129);
		for(int i = 1; i<129; i++){
			xr[0] = a;
			xr[i] += xr[i-1] + dx;
			}
				
		vector[] neural = nn.network_d(xr, n, p:train, a:a, b:b);
		vector model = neural[0];
		vector diff = neural[1];
		vector antidiff = neural[2];
		for(int i = 0; i<model.size; i++){
			Bplot.Write($"{xr[i]} {model[i]} {diff[i]} {antidiff[i]}\n");
			}
		Bplot.Close();
	}
}
