using System;
using System.IO;
using static System.Math;
using static System.Console;
class main{
	static void Main(){
		int n = 5;
		
		//Activation function
		Func<double,double> f = (x) => x*Exp(-x*x);
		
		//function to fit
		//Func<double,double> g = (x) => Cos(5*x-1)*Exp(-x*x);
		Func<double,double> g = (x) => Cos(x*x);		

		int nx = 20;
		vector xs = new vector(nx);
		vector ys = new vector(nx);
		
		double a = -1;
		double b = 1;
		double w = 1;
		
		
		var Aplot = new StreamWriter("Aplot.txt");
		
		for(int i = 0; i<nx; i++){
			xs[i] = a+(b-a)*i/(nx-1);
			ys[i] = g(xs[i]);
			Aplot.Write($"{xs[i]} {ys[i]}\n");
			}
		
		// now training the function
		// *Do remember to change the steplim. 
		// Steplim has a huge impact upon the accuracy of the minimization
		vector train = nn.train(xs,ys, n, f, steplim:9999);
		
		train.print("p new = ");
		
		Aplot.Write("\n\n");
		
		// building the network
		;// = new vector(2*(1/64));
		
		double dx = b/64;
		vector xr = new vector(129);
		for(int i = 1; i<129; i++){
			xr[0] = a;
			xr[i] += xr[i-1] + dx;
			}
				
		vector model = nn.network(xr, n, f, p:train);
		for(int i = 0; i<model.size; i++){
			Aplot.Write($"{xr[i]} {model[i]}\n");
			}
		Aplot.Close();
	}
}
