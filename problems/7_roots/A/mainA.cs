using System;
using static System.Console;
using static System.Math;
using System.IO;
class main{
	static void Main(){
		
		
		vector xstart = new vector(1);
		xstart[0] = 0;
		
		// Finding the roots of these two equations.
		Func<vector,vector> f = (x) => new vector(Cos(x[0]));
		Func<vector,vector> g = (x) => new vector(Pow(Sin(x[0]),3));
		
		// Creating a file for a plot of roots vs function
		var Aplot = new StreamWriter("Aplot.txt");
		int n = 10;
		for(int i=1;i<n;i+=1){
			xstart[0] = i;
			vector roots1 = root.newton(f,xstart,1e-3);	
			Aplot.Write($"{roots1[0]} {i-i}\n");
			}
		Aplot.Write("\n\n");
		for(double i = 0; i<n;i+=0.1){
			double cos = Cos(i);
			Aplot.Write($"{i} {cos}\n");
		}
		Aplot.Write("\n\n");
		for(int i=1;i<n;i+=1){
			xstart[0] = i;
			vector roots2 = root.newton(g,xstart,1e-3);
			Aplot.Write($"{roots2[0]} {i-i}\n");
			}
		Aplot.Write("\n\n");
		for(double i = 0; i<n; i+=0.1){
			double sin = Pow(Sin(i),3);
			Aplot.Write($"{i} {sin}\n");
			}
		Aplot.Close();
		
		// In the Aplot.svg the verification of the newton method can be seen. 

		// Now finding the extremums of the rosenbrock valley function

		/*Func<vector,vector> R = (x,y) => Pow(1-x[0],2) + 100 * Pow(y[0]-Pow(x[0],2),2);
		^does not work. But assigning vector R[0] as x, and R[1] as y, should work.
		But first the derivatives of R'(x) and R'(y) has to be found:
		
		R'(x) = -2+2*x-400*(-x^2 + y)*x
		R'(y) = -200*x^2 + 200*y
		 */
		
	//	Func<vector,vector> h = (R) =>new vector(-2+2*R[0]-400*(Pow(-R[0],2)+R[1])*R[0], -200*Pow(R[0],2)+200*R[1]);
		
		//Trying another approach in writing the function
		int calls = 0;
		Func<vector,vector> h = delegate(vector R){
			calls++;
			vector r=new vector(2);
			double x = R[0], y = R[1];

			/*r[0] = -2+2*x-400*(Pow(-x,2)+y)*x; - Apparently something 
			 goes wrong, when I insert the function like that.
			 Therefore I've found someone who put it in differently, which
			 works */
			r[0] = 2*(1-x)+2*100*(y-x*x)*(-2*x);
			//r[1] = -200*Pow(x,2)+200*y;
			r[1] = 100*2*(y-x*x);
			return r;
		};
		
		

		// The analytical minimum is at (1,1).
		// Starting off with the Jacobian matrix:
		
		vector xstart1 = new vector(2);
		xstart1[0] = 0.5;
		xstart1[1] = 0.7;
		matrix J = root.Jacobian(h, xstart1);
		WriteLine("Find the extremum(s) of the RosenBrock's valley function");
		WriteLine("\nStart of by showing the Jacobian matrix:");
		J.print("J=");		
				
		double eps = 1e-3;
		vector roots = root.newton(h,xstart1,eps);
		
		WriteLine("The analytical solution for the roots is [1,1] :");
		roots.print("found roots =");
		WriteLine($"\nThe deviation is = {roots[0]-1} {roots[1]-1}");
		WriteLine($"The number of calls = {calls}");
		WriteLine($"Do these roots pass the test?");
		WriteLine($"Test passed if ||f(roots)||<eps : {h(roots).norm()} < {eps}");
		if(h(roots).norm()<eps) WriteLine("test passed");
		else	WriteLine("test failed");
	}
}
