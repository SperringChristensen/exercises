using System;
using System.Collections;
using System.Collections.Generic;
public class hydrogen{
	public static double Fe(double e, double r){
		double rmin = 1e-3;
		if(r<rmin) return r-r*r;
		
		Func<double,vector,vector> wave = (x,y) =>{ 
		return new vector(y[1], 2*(-1/x-e)*y[0]);};
		// f(r->0) = r-r*r, f'(r->0) = 1-2*rmin
		vector yrmin = new vector(rmin-rmin*rmin, 1-2*rmin);
		double h = 1e-3;

		/* So for this assignment, my ODE apparently doesn't work.
		I've tried to debug it, and copied some more from your ODE solver,
		but that also doesn't work... So I'm just going forward with using your.

		**The funny thing is tho', that my ODE solver works for every other assignment I've done.. */

		vector yrmax = ode.rk23(wave, rmin, yrmin, r, h:h);
		return yrmax[0];
	}
}
