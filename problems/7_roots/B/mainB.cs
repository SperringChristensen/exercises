using System;
using System.IO;
using static System.Math;
class main{
	static void Main(){
		double rmax = 8.0;
		
		Func<vector,vector> m = (vector v) =>{
			double e=v[0];
			double frmax = hydrogen.Fe(e,rmax);
			return new vector(frmax);
			};
				
		vector vstart = new vector(-1.0);
		vector vroot = root.newton(m, vstart, 1e-4);
		vroot.print();
		double energy = vroot[0];
	
		var Bplot = new StreamWriter("Bplot.txt");
		for(double i=0; i<=rmax; i+=rmax/128)
			Bplot.Write($"{i} {hydrogen.Fe(energy,i)} \n");
		
		
		Bplot.Write("\n\n");
		for(double i = 0; i<=rmax; i+=0.1){
			double exp = i*Exp(-i);
			Bplot.Write($"{i} {exp}\n");
		}
		Bplot.Close();	
	}
}
