using System;
public class Matrix2x2{
	private double [,] A = new double [2,2];
	public Matrix2x2(){
		for ( int i = 0; i<2; i++)
			for ( int j = 0; j<2; j++)
				A[i,j] = 0;
		}
	public double this[int i, int j]{
		get{
			if (!((i < 0 || i>=2) && (j < 0 || j <=2)))
				return A[i,j];
			else
				return 0;
			}
		set{
			if (!((i < 0 || i >=2) && (j < 0 || j <= 2)))
				A[i,j] = value;
		}
	}
	public double Determinant(){
		double d = 0;
		d += A[0, 0] * A[1, 1];
		d -= A[0, 1] * A[1, 0];
		return d;
	}
	
	public void Copy(Matrix2x2 X){
		for (int i=0;i<2;i++)
			for (int j=0;j<2;j++)
				this[i, j] = X[i, j];
	}
	
	public static int ChangeColumn(out Matrix2x2 R, Matrix2x2 A, Vector2 b, int j){
	R = new Matrix2x2();
	R.Copy(A);
	
	if(j<0||j>=2)
		return 1;
	
	R[0, j] = b[0];
	R[1, j] = b[1];
	
	return 0;
	}
	
	public static int SolveEquations(out Vector2 x, Matrix2x2 A, Vector2 b){
		x = new Vector2();
		const double eps = 1.0E-12;
		double d = A.Determinant();
		if (Math.Abs(d) < eps)
			return 1;

		Matrix2x2 M0 = new Matrix2x2();
		Matrix2x2 M1 = new Matrix2x2();

		ChangeColumn(out M0, A, b, 0);
		ChangeColumn(out M1, A, b, 1);

		x[0]= M0.Determinant() /d;
		x[1]= M1.Determinant() /d;

		return 0;
	}

}
