public class Vector2{
	private double[] v = new double[2];
	public Vector2(){
	v[0] = v[1] = 0;
	}
	public double this[int i]{
		get{
			if (i<0 || i>=2)
			return 0;
		else
			return v[i];
		}
		set{
			if(!(i<0 || i>=2))
			v[i]=value;
		}
	}
	public double x{
		get { return v[0];}
		set{v[0]=value;}
	}
	public double y{
		get {return v[1];}
		set{v[1]=value;}
	}

}
