using System;
using static System.Console;
using static System.Math;
using static cmath;
class main{
static void Main()
{
	// Set coefficient values
	Matrix3x3 A = new Matrix3x3();
	A[0, 0] = 1; A[0, 1] = 1; A[0, 2] = -1;
	A[1, 0] = 0.2; A[1, 1] = -0.2; A[1, 2] = 0;
	A[2, 0] = 0; A[2, 1] = 0.2; A[2, 2] = 0.75;
	
	// declare a vector of right-hand side
	Vector3 b = new Vector3();
	
	// set constants on the right-hand side
	b[0] = 0; b[1] = -0.2; b[2] = 3.5;

	// declare a solution vector
	Vector3 x;

	//solve the system
	if (Matrix3x3.SolveEquations(out x, A, b) == 0){
		//put down results
		WriteLine("x[0] = {0:G}",x[0]);
		WriteLine("x[1] = {0:G}",x[1]);
		WriteLine("x[2] = {0:G}",x[2]);
	}
	else{
		WriteLine("Solution not found");
	}

}
}
