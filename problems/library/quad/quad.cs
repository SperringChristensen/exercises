using System;
using static System.Math;
public static class quad{
		//building a recursive adaptive integrator:
		//this is the trapozoidal rule with 4 points
	public static double trapintegrate(Func<double,double> f, double a, double b, double acc=1e-6, double eps=1e-6, double f2 = System.Double.NaN, double f3 = System.Double.NaN, int nrec=0, int limit=10){
		
			double f1 = f(a+(b-a)/6);
			double f4 = f(a+5*(b-a)/6);
			
			double Q = (2*f1+f2+f3+2*f4)/6*(b-a);
			double q = (f1+f4+f2+f3)/4*(b-a);
			
			double tolerance = acc+eps*Abs(Q);
			double error = Abs(Q-q);
						
			if(System.Double.IsNaN(f2)){
				f2 = a+2*(b-a)/6;
				f3 = a+4*(b-a)/6;
			}
			
			

			if(error<tolerance) return Q;
			else {
				double Q1 = trapintegrate(f,a,(a+b)/2,acc/Sqrt(2),eps,f1,f2,nrec+1);
				double Q2 = trapintegrate(f,(a+b)/2,b,acc/Sqrt(2),eps,f3,f4,nrec+1);
				return Q1+Q2;}	
			

		}

		// Gauss rules for large n are is expensive in computing power to construct
		// therefore Clenshaw-Curtis rules can be used
		
	public static double CC(Func<double,double> f, double a, double b, double acc=1e-6, double eps = 1e-6){
			// First, I need to rescale from -1 to 1, to 0-1

			//I ofc need to fix the scaling properly. This will be done later
			double a1 = 0, b1 = PI;
			if(a==0) b1 = b1/2;
			
			
			Func<double,double> C = theta => f(Cos(theta))*Sin(theta);

			// now using the trapezoidal integrator
			return trapintegrate(C,a1,b1,acc,eps);		
			
		}
		
	public static double integrate_inf(Func<double,double> f, double a , double b , double acc=1e-6, double eps = 1e-6){
			// The equation I will be using is:
			// inf_-inf^+inf f(x) dx = inf_-1^+1 f(t/(1-t^2))*(1+t^2)/(t-t^2)^2 dt
			
			// Defining the different functions
			double a1,b1;			
			// For -inf to +inf
			Func<double,double> f1 = (t) => (f((1-t)/t)+f(-((1-t)/t))) * 1/Pow(t,2);
			// for a to +inf
			Func<double,double> f2 = (t) => f(a+(1-t)/t) * 1/Pow(t,2);
			//for -inf to +b
			Func<double,double> f3 = (t) => f(b-((1-t)/t)) * 1/Pow(t,2);

			if(a == double.NegativeInfinity ){
			a1 = 0;
			b1 = 1;
			return trapintegrate(f1,a1,b1,acc,eps);
			}
			if(a != double.NegativeInfinity){
			a1 = 0;
			b1 = 1;
			return trapintegrate(f2,a1,b1,acc,eps);
			}
			if(b != double.PositiveInfinity){
			a1 = 0;
			b1 = 1;
			return trapintegrate(f3,a1,b1,acc,eps);		
			}
			else{
				return 1;
			}

			//return trapintegrate(f1,a1,b1,acc,eps);
		
			
			

			}
}
