using System;
using static System.Math;
public class cspline{
	double[] x,y,b,c,d;
	// creating the spline
	public cspline(double[] xr, double[]yr){
		int n = xr.Length;
		x=new double[n];
		y=new double[n];
		for(int i=0;i<n;i++)
			{x[i]=xr[i];y[i]=yr[i];}
		b=new double[n];
		c=new double[n-1];
		d=new double[n-1];
		var deltax = new double[n-1];
		var p = new double[n-1];
		for(int i=0;i<n-1;i++){
			deltax[i] = x[i+1] -x[i];
			p[i] = (y[i+1]-y[i])/deltax[i];
			}
		// now i have to build the tridiagonal system, as seen in the chapter about cubic spline
		var D = new double[n];
		var Q = new double[n-1];
		var B = new double[n];
		D[0]=2;
		for(int i=0;i<n-2;i++){
			D[i+1]=2*deltax[i]/deltax[i+1] + 2;
			}
		D[n-1]=2;
		Q[0]=1;
		for(int i=0;i<n-2;i++){
			Q[i+1]=deltax[i]/deltax[i+1];
			}
		for(int i=0;i<n-2;i++){
			B[i+1]=3*(p[i]+p[i+1]*deltax[i]/deltax[i+1]);
		}
		B[0] = 3*p[0];
		B[n-1]=3*p[n-2];
		// Gauss elimination : 
		for(int i=1;i<n;i++){
			// -= is quivalent to C = C-A
			D[i]-=Q[i-1]/D[i-1];
			B[i]-=B[i-1]/D[i-1];
			}
		b[n-1]=B[n-1]/D[n-1];
		for(int i=n-2;i>=0;i--){
			b[i]=(B[i]-Q[i]*b[i+1])/D[i];
			}
		for(int i=0;i<n-1;i++){
			c[i]=(-2*b[i]-b[i+1]+3*p[i])/deltax[i];
			d[i]=(b[i]+b[i+1]-2*p[i])/deltax[i]/deltax[i];
			}
		}
	public double eval(double z){
		int i = binsearch.search(x,z);
		double dx = z-x[i];
		return y[i]+dx*(b[i]+dx*(c[i]+dx*d[i]));
		}
	public double deriv(double z){
		int i = binsearch.search(x,z);
		double dx = z-x[i];
		return b[i]+2*c[i]*dx+3*d[i]*Pow(dx,2);
		}
	public double integ(double z){
		int iz = binsearch.search(x,z);
		double sum=0,dx;
		for(int i=0;i<iz;i++){
			dx=x[i+1]-x[i];
			sum+=1/4*Pow(dx,4)*d[i]+1/3*Pow(dx,3)*c[i]+1/2*Pow(dx,2)*b[i]+y[i]*dx;
			}
		dx=z-x[iz];
		sum+=1/4*Pow(dx,4)*d[iz]+1/3*Pow(dx,3)*c[iz]+1/2*Pow(dx,2)*b[iz]+y[iz]*dx;
		return sum;
		}

}
