using System;
using static System.Math;
public static class lspline{
	public static double linterp(double[] x, double[] y, double z){
		int i= binsearch.search(x, z);
		return y[i]+(((y[i+1]-y[i])/(x[i+1]-x[i]))*(z-x[i]));
	}
	public static double linterpInteg(double[] x, double[] y, double z){
		int iz = binsearch.search(x, z);
		double sum=0,dx,p,dz;
		// calculating the integral without z
		// integrating dx yields => int 1 dx => 0.5dx^2
		for(int i=0;i<iz;i++){
			dx = x[i+1]-x[i];
			p = (y[i+1]-y[i])/dx;
			sum+=y[i]*dx+p*0.5*Pow(dx,2) ;	
		}
		// now inserting dxz => dx = (z-x)
		dz = z-x[iz];
		double pz = (y[iz+1]-y[iz])/(x[iz+1]-x[iz]);
		sum += y[iz] * dz + pz*0.5 *Pow(dz,2) ;	
		return sum;
	}
}

