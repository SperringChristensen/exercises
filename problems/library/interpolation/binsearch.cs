using System;

public static class binsearch{
	public static int search(double[] x, double z){
		int i=0, j=x.Length-1;
		while(j-i>1){int mid=(i+j)/2; if(z>x[mid]) i=mid; else j=mid;}
		return i;
		}
}
