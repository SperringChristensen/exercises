using System;
using static System.Math;
public partial class jacobi{
	public static int cyclic(matrix A, vector e, matrix V = null){
	// Jacobi diagonalization: upper triangle of A is destroyed
	// e and V accumulate eigenvalues and eigenvectors
	int sweeps=0, n=A.size1;
	bool changed; // this bool decides when the loop ends
	//var e = new vector(n);
	var B = new matrix(n,n);
	B = A.copy();
	// creating vector e
	for(int i=0;i<n;i++){
		e[i] = B[i,i];
		}
	// This one is really important, I unfortuneately had too copy
	// it from your jacobi.cs.
	// The rest of the code is from the literature given.
	// This code inserts q,p into V.
	if(V!=null) for(int i=0;i<n;i++){
		V[i,i] = 1.0; for(int j=i+1;j<n;j++){
			V[i,j]=0;
			V[j,i]=0;
			}
		}
	do{ changed = false; sweeps++; int p,q;
		//for(p=0;p<n;p++){
		//for(q=p+1;q<n;q++){
		for(q=n-1;q>0;q--){
		for(p=0;p<q;p++){
			double app = e[p];
			double aqq = e[q];
			double apq = B[p,q];
			double phi = 0.5*Atan2(2*apq,aqq-app);
			double c = Cos(phi), s = Sin(phi);
			double app1 = c*c*app-2*s*c*apq+s*s*aqq;
			double aqq1 = s*s*app+2*s*c*apq+c*c*aqq;
			if(app1!=app || aqq1!=aqq){
				changed = true;
				e[p] = app1;
				e[q] = aqq1;
				B[p,q] = 0;
				for(int i=0;i<p;i++){
					double aip = B[i,p];
					double aiq = B[i,q];
					B[i,p] = c*aip-s*aiq;
					B[i,q] = c*aiq+s*aip;
					}
				for(int i=p+1;i<q;i++){
					double api = B[p,i];
					double aiq = B[i,q];
					B[p,i] = c*api-s*aiq;
					B[i,q] = c*aiq+s*api;
					}
				for(int i=q+1;i<n;i++){
					double api = B[p,i];
					double aqi = B[q,i];
					B[p,i] = c*api-s*aqi;
					B[q,i] = c*aqi+s*api;
					}
				if(V!=null)for(int i=0;i<n;i++){
					double vip = V[i,p];
					double viq = V[i,q];
					V[i,p] = c*vip-s*viq;
					V[i,q] = c*viq+s*vip;
					}
					// This stops the loop, if
					// it hasen't changed
				} }} } while(changed);
					return sweeps;
	}
	public static int onerow(matrix A, vector e, int nvalues=1, matrix V = null){
	// Jacobi diagonalization: upper triangle of A is destroyed
	// e and V accumulate eigenvalues and eigenvectors
	// onerow only finds the lowest eigenvalue.
	int sweeps=0, n=A.size1, rotations = 0;
	bool changed; 
	//var e = new vector(n);
	var B = new matrix(n,n);
	B = A.copy();
	// creating vector e
	for(int i=0;i<n;i++){
		e[i] = B[i,i];
		}
	// This one is really important, I unfortuneately had too copy
	// it from your jacobi.cs.
	// The rest of the code is from the literature given.
	// This code inserts q,p into V.
	if(V!=null) for(int i=0;i<n;i++){
		V[i,i] = 1.0; for(int j=i+1;j<n;j++){
			V[i,j]=0;
			V[j,i]=0;
			}
		}
	for(int p=0;p<nvalues;p++)do{ 
		changed = false; int q;
		//for(p=0;p<n;p++){
		for(q=p+1;q<n;q++){
		//for(q=n-1;q>0;q--){
		//for(p=0;p<q;p++){
			double app = e[p];
			double aqq = e[q];
			double apq = B[p,q];
			double phi = 0.5*Atan2(2*apq,aqq-app);
			double c = Cos(phi), s = Sin(phi);
			double app1 = c*c*app-2*s*c*apq+s*s*aqq;
			double aqq1 = s*s*app+2*s*c*apq+c*c*aqq;
			if(app1!=app || aqq1!=aqq){
				rotations++;
				changed = true;
				e[p] = app1;
				e[q] = aqq1;
				B[p,q] = 0;
				//for(int i=0;i<p;i++){
				//	double aip = B[i,p];
				//	double aiq = B[i,q];
				//	B[i,p] = c*aip-s*aiq;
				//	B[i,q] = c*aiq+s*aip;
				//	}
				for(int i=p+1;i<q;i++){
					double api = B[p,i];
					double aiq = B[i,q];
					B[p,i] = c*api-s*aiq;
					B[i,q] = c*aiq+s*api;
					}
				for(int i=q+1;i<n;i++){
					double api = B[p,i];
					double aqi = B[q,i];
					B[p,i] = c*api-s*aqi;
					B[q,i] = c*aqi+s*api;
					}
				if(V!=null)for(int i=0;i<n;i++){
					double vip = V[i,p];
					double viq = V[i,q];
					V[i,p] = c*vip-s*viq;
					V[i,q] = c*viq+s*vip;
					}
					// This stops the loop, if
					// it hasen't changed
				} }}	while(changed);
					return rotations;
	}
	// This could also be a part of the previous code, but are too lazy to implement it. 
	public static int onerowlargest(matrix A, vector e, int nvalues=1, matrix V = null){
	// Jacobi diagonalization: upper triangle of A is destroyed
	// e and V accumulate eigenvalues and eigenvectors
	// onerow only finds the lowest eigenvalue.
	int sweeps=0, n=A.size1, rotations = 0;
	bool changed; 
	//var e = new vector(n);
	var B = new matrix(n,n);
	B = A.copy();
	// creating vector e
	for(int i=0;i<n;i++){
		e[i] = B[i,i];
		}
	// This one is really important, I unfortuneately had too copy
	// it from your jacobi.cs.
	// The rest of the code is from the literature given.
	// This code inserts q,p into V.
	if(V!=null) for(int i=0;i<n;i++){
		V[i,i] = 1.0; for(int j=i+1;j<n;j++){
			V[i,j]=0;
			V[j,i]=0;
			}
		}
	for(int p=0;p<nvalues;p++)do{ 
		changed = false; int q;
		//for(p=0;p<n;p++){
		for(q=p+1;q<n;q++){
		//for(q=n-1;q>0;q--){
		//for(p=0;p<q;p++){
			double app = e[p];
			double aqq = e[q];
			double apq = B[p,q];
			// double phi = 0.5*Atan2(2*apq,aqq-app);
			double phi = 0.5*Atan2(-2*apq,app-aqq);
			double c = Cos(phi), s = Sin(phi);
			double app1 = c*c*app-2*s*c*apq+s*s*aqq;
			double aqq1 = s*s*app+2*s*c*apq+c*c*aqq;
			if(app1!=app || aqq1!=aqq){
				rotations++;
				changed = true;
				e[p] = app1;
				e[q] = aqq1;
				B[p,q] = 0;
				//for(int i=0;i<p;i++){
				//	double aip = B[i,p];
				//	double aiq = B[i,q];
				//	B[i,p] = c*aip-s*aiq;
				//	B[i,q] = c*aiq+s*aip;
				//	}
				for(int i=p+1;i<q;i++){
					double api = B[p,i];
					double aiq = B[i,q];
					B[p,i] = c*api-s*aiq;
					B[i,q] = c*aiq+s*api;
					}
				for(int i=q+1;i<n;i++){
					double api = B[p,i];
					double aqi = B[q,i];
					B[p,i] = c*api-s*aqi;
					B[q,i] = c*aqi+s*api;
					}
				if(V!=null)for(int i=0;i<n;i++){
					double vip = V[i,p];
					double viq = V[i,q];
					V[i,p] = c*vip-s*viq;
					V[i,q] = c*viq+s*vip;
					}
					// This stops the loop, if
					// it hasen't changed
				} }}	while(changed);
					return rotations;
	}
	
	
}		
		

