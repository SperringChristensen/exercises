using System;
using static System.Console;
using static System.Math;
public partial class quasi{
		// now building the gradiant vector,
		
	
		// Apparently my own gradient finder doesn't work. Can't figure out why..
		// Don't min it being called hessian, that will be changed
		public static readonly double EPS=1.0/4194304;

		public static vector hessian(Func<vector,double>f, vector x){
		vector g=new vector(x.size);
			double fx=f(x);
			for(int i=0;i<x.size;i++){

			double dx=Abs(x[i])*EPS;
			if(Abs(x[i])<Sqrt(EPS)) dx=EPS;
			x[i]+=dx;
			g[i]=(f(x)-fx)/dx;
			x[i]-=dx;
		}
		return g;
		}

		// This is just gonna be like the root finder.
		public static vector[] min(Func<vector,double> f, vector xstart, double eps = 1e-6,double acc = 1e-6, int steplim = 999999){
			
			/* General equation here is (B+deltaB)*y=s, with:
			B = H^-1, y = napla phi(x+s) - napla phi(x), and u = s - B*y
			(napla = gradient) */
			
			vector x = xstart.copy();
			double fx = f(x);
			vector h = hessian(f, x);
			int n = x.size;
			double dx = EPS;
						
			//stealing this one from Dmitri:
			// This is to become the inverse hessian matrix
			matrix B = matrix.id(n);
			
			// Setting the steps
			int steps = 0;
			while(true){
				steps++;

				// calculating dx
				vector Dx = -B * h;
				
					if(Dx.norm()<dx*x.norm()){
						Error.Write($" dx.norm() {Dx.norm()} < x.norm() {x.norm()}\n");
						break;
						}
					if(h.norm()<acc){
						Error.Write($"h.norm() {h.norm()} < acc {acc}\n");
						break;
						}
						
						
					vector y;
					double fy;
					double lambda = 1;
					// now creating the backtracking linesearch
					
					while(true){
						y = x+Dx*lambda;
						fy = f(y);
						if(fy<fx) break;
						if(lambda<EPS){
							//now setting the matrix
							B.setid();
							break;
						}
						else lambda/=2;
					}

									
					// smaller step s = lambda*deltax
					vector s = y-x;
						
					//gradient of y
					vector hy = hessian(f, y);
						
					// I've unfortunately already used y, therefore y = z = napla phi(x+s);
					vector z = hy - h;
						
					// Now calculating the updater:
					
					vector u = s - B*z;

					/* using the boyden's update:
					deltaB = c*s^T, where vector c is found from :
					c = u/s.dot(y) 
					Sometimes the dot product becomes very small, 
					which can be avoided by
					only beforming updates if the condition |s.dot(y)| > eps*/
					
					double epsdot = 1e-6;
								
					
					// now using broyden's update
					
					if(Abs(s.dot(y)) > epsdot){
						vector c = u/s.dot(y);
						B.update(u,u,1/c[0]);
						}
						
						
					x = y;
					h = hy;
					fx = fy;
					
					if(steps>steplim) break;
					
					}
			vector vsteps = new vector(steps);
			vector[] result={x,vsteps};
			return result;
		}
					
			
}

