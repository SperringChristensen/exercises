using System;
using static System.Math;
public static class monte{
	public static vector[] plainmcs(Func<vector,double> f, vector a, vector b, int N){
			
			//Building random x
			var rand = new Random();
			/*vector randomx = new vector(a.size);
			for(int i = 0; i<a.size; i++){
				randomx[i] = a[i] + rand.NextDouble()*(b[i]-a[i]);
			}
			*/
			
			//Need to be as a function	
			Func<vector> randomx = delegate(){
				vector x = new vector(a.size);
				for(int i = 0; i<x.size; i++)
					x[i] = a[i] + rand.NextDouble()*(b[i]-a[i]);
				return x;
			};

			double V =1;
			for(int i=0; i<a.size; i++)
				V*=b[i]-a[i];
			
			double sum=0, sum2=0;
			
			for(int i=0; i<N; i++){
				vector x = randomx();;
				double fx = f(x);
				sum+=fx;
				sum2+=fx*fx;		
				}
			
			double avr = sum/N; //mean (average)
			double variance = sum2/N-avr*avr; //sigma = variance
			double sigma = Sqrt(variance);
			double SIGMA = sigma/Sqrt(N);
			
			vector result = new vector(avr*V);
			vector err = new vector(SIGMA*V);
			
			vector[] vout = {result,err};
			

			return vout;
		}
}
			
