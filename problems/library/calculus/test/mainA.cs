using System;
using System.IO;
using static System.Math;
using static System.Console;
class main{
	static void Main(){
		int n = 5;
		
		//Activation function
		Func<double,double> f = (x) => x*Exp(-x*x);
		
		//function to fit
		//Func<double,double> g = (x) => Cos(5*x-1)*Exp(-x*x);
		Func<double,double> g = (x) => Cos(x);		

		int nx = 20;
		vector xs = new vector(nx);
		vector ys = new vector(nx);
		
		double a = -1;
		double b = 1;
		double w = 1;
		
		
		var Aplot = new StreamWriter("Aplot.txt");
		
		for(int i = 0; i<nx; i++){
			xs[i] = a+(b-a)*i/(nx-1);
			ys[i] = g(xs[i]);
			Aplot.Write($"{xs[i]} {ys[i]}\n");
			}
		
		Aplot.Write("\n\n");
		vector y = calc.derivative(g, xs);
		
		//vector sin = new vector(xs.size);
		
		for(int i = 0; i<nx; i++){
			double sin = -Sin(xs[i]);		
			Aplot.Write($"{xs[i]} {y[i]} {sin}\n");
			}
			
		Aplot.Close();
	}
}
