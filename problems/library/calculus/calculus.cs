using System;
using static System.Math;
public class calc{
	public static vector derivative(Func<double,double> f, vector xs, double hstepper = 1e-6){
		double h = hstepper;
		
		vector x = xs.copy();
		vector y = new vector(x.size);
		for(int i = 0; i<x.size; i++){
			y[i] = (f(x[i]+h)-f(x[i]-h))/(2*h);
			}
		return y;
	}

}
			
