using System;
using static System.Math;
public class nn{
/*Starting off with creating the general class, with n = number of hidden neurons.
This script is heavly inspired by Dmitri's :)*/
	public static vector network(vector xs, int n, Func<double,double> f, vector p = null , double a = -1, double b = 1, double w = 1){
		
		// a is start, b is end
		
		//Defining
		vector sum = new vector(xs.size);
		double fa = 0, fb = 0, fw = 0;
		
		// building the weight vector
		int wp = 3; // have three parameters
		if(p == null){
			p = new vector(wp*n);
		for(int k = 0; k<xs.size; k++){
		for(int i = 0; i<n;i++){
			
			p[wp*i+0] = a+(b-a)*i/(n-1);
			p[wp*i+1] = b;
			p[wp*i+2] = w;
			fa = p[wp*i+0];
			fb = p[wp*i+1];
			fw = p[wp*i+2];
				// Weighted output signal : y=f((x-a)/b)*w
			sum[k] += fw*f((xs[k]-fa)/fb);
			}
			}
		
		}
		else if(p != null){
			for(int k = 0;k<xs.size;k++){
			for(int i = 0; i<n;i++){
				fa = p[wp*i+0];
				fb = p[wp*i+1];
				fw = p[wp*i+2];
				sum[k] += fw*f((xs[k]-fa)/fb);
				}
				}
			}
		return sum;
		}
		
		//Network with derivative
		public static vector[] network_d(vector xs, int n, vector p = null, double xb = 0 , double a = -1, double b = 1, double w = 1){
		//With known activation function:
		
		//My original thought was to build my own diff and integral solver, for indefined integrals
		// with the richt algorithm
		Func<double,double> f = (x) =>x*Exp(-x*x);
		Func<double,double> fd = (x) => Exp(-x*x) - 2*x*x * Exp(-x*x);
		Func<double,double> fi = (x) => -0.5 *Exp(-x*x);
		
		
		// a is start, b is end
		
		//Defining
		vector sum = new vector(xs.size);
		vector diffsum = new vector(xs.size);
		vector intsum = new vector(xs.size);
		vector intlowsum = new vector(xs.size);
		vector isum = new vector(xs.size);
		double fa = 0, fb = 0, fw = 0;
		
		// building the weight vector
		int wp = 3; // have three parameters
		if(p == null){
			p = new vector(wp*n);
		for(int k = 0; k<xs.size; k++){
		for(int i = 0; i<n;i++){
			
			p[wp*i+0] = a+(b-a)*i/(n-1);
			p[wp*i+1] = b;
			p[wp*i+2] = w;
			fa = p[wp*i+0];
			fb = p[wp*i+1];
			fw = p[wp*i+2];
				// Weighted output signal : y=f((x-a)/b)*w
			sum[k] += fw*f((xs[k]-fa)/fb);
			}
			}
		
		}
		else if(p != null){
			for(int k = 0;k<xs.size;k++){
			for(int i = 0; i<n;i++){
				fa = p[wp*i+0];
				fb = p[wp*i+1];
				fw = p[wp*i+2];
				sum[k] += fw*f((xs[k]-fa)/fb);
				diffsum[k] += fw*fd((xs[k]-fa)/fb)/fb; //chain rule
				intsum[k] += fw*fi((xs[k]-fa)/fb)*fb;
				intlowsum[k] += fw*fi((xb-fa)/fb)*fb;
				isum[k] = intsum[k] - intlowsum[k];
				
				}
				}
			}
			
		//The derivative 
		
		vector[] result = {sum,diffsum,isum};
		return result;
		}
		
		
		
		
		
	//Now building the train

	public static vector train(vector xs, vector ys, int n, Func<double,double> f, double a = -1, double b = 1, double w =1, int steplim = 999){
		// First defining the amount of calls the routine has to go through
		int calls = 0; int wp = 3;
		vector p = new vector(wp*n);
		for(int i = 0; i<n;i++){
			//while(i<=n){
			p[wp*i+0] = a+(b-a)*i/(n-1);
			p[wp*i+1] = b;
			p[wp*i+2] = w;
				}
		
		
		
		//Before I can minimize, I need to create a function
				
		//Building the network
		vector neu = network(xs, n, f,p);

		Func<vector,double> err = (v) =>{
			calls++;
			p=v;
			double errsum = 0;
			for(int k = 0; k<xs.size;k++){
				errsum+= Pow(network(xs,n,f,p)[k]-ys[k],2);
			}
			return errsum/xs.size;
			};
		double eps = 1e-6;
		double acc = 1e-6;
					
		vector[] result = quasi.min(err, p, eps,acc ,steplim);
		vector mini = result[0];
		return mini;
	}
	
	
	
	
	
	

}
