using System;
using static System.Console;
using static System.Math;
using System.Collections.Generic;
public partial class rk{
	public static vector[] rkstep12(Func<double,vector,vector> F, double t, vector yt, double h){
		// only solves one step at a time, yi+1 = yi+h*k
		// this is the mid-point euler method
		// also known as second-order unga-kutta
		// it simply just adds a mid point in the step. 
		// this only takes 1step.
		vector k0 = F(t, yt);
		vector k12 = F(t+1/2*h,yt + 1/2*h*k0);
		vector k = k12;
		vector yh = yt+k*h;
		vector er = (k12-k0)*h;
		vector[] result={yh,er};
	return result;
	}
	public static vector rk12(Func<double,vector,vector> F, 
		double a, vector yt, double b, double h, List<double> ts=null, List<vector> ys = null, 
			double acc=1e-3, double eps=1e-3, int limit=999){
			return driver(rkstep12,F,a,yt,b,h,acc,eps,ts,ys);
			}
	
	public static vector[] rkstep23(Func<double,vector,vector> F, double t, vector yt, double h){
	//FUNC = the right-hand-side of dy/dt=f(t,y)
	// t = the current value of the variable
	// vector yt = the current value y(t) of the sought function
	// h = step to be taken
	// yh = output y(t+h)
	// vector err = output error
	
	// this is the runge kutta second-third order solver.
		vector k0 = F(t,yt);
		vector k1 = F(t+h/2, yt+(h/2)*k0);
		vector k2 = F(t+3*h/4, yt+(3*h/4)*k1);
		vector ka = (2*k0+3*k1+4*k2)/9;
		vector kb = k1;
		vector yh = yt+ka*h;
		vector er = (ka-kb)*h;
		vector[] result={yh,er};
	return result;
	}
	
	public static vector rk23(Func<double,vector,vector> F, 
		double a, vector yt, double b, double h, List<double> ts=null, List<vector> ys = null, 
			double acc=1e-3, double eps=1e-3, int limit=999){
			return driver(rkstep23,F,a,yt,b,h,acc,eps,ts,ys);
			}
	
	// Now building the driver
	// this is the step-size controller, with tolerance tau
	// which is the maximum accepted error
	// then taui = tau * sqrt(h/(b-a))
	public static vector driver(Func<Func<double,vector,vector>,double,vector,double,vector[]> stepper,
			Func<double,vector,vector> f,
			double a, vector yt, double b, double h, //step size
			double acc, double eps, 
			List<double> ts = null, List<vector> ys = null, int limit=999){
			// setting starting point
			double t = a;
			
			// Filling out the lists
			if(ts!=null) ts.Add(t);
			if(ys!=null) ys.Add(yt);
			vector[] step;
			int nsteps = 0;
		
			//choosing to do it as a while loop
			while(t<=b){
				nsteps++;
				// this had to bee inserted after the roots  B assignment
				if(t>=b) {return yt; }
				if(nsteps>limit) {Error.Write($"driver nsteps>{limit}\n");
					return yt;}
				
				
				if(t+h>b) h=b-t;
				// defining the stepper
				step = stepper(f,a,yt,h);
				
				// making the yi+1 and error vectors.
				vector yh = step[0];
				vector er = step[1];			
				
				// creating the local tolerance for-loop
				vector tol = new vector(er.size);
				for(int i=0; i<tol.size; i++){
					// deltayi = |yi|*eps => estimate of integration error at i
					// tua i = deltayi * sqrt(h/(b-a))
					tol[i]=Max(acc,Abs(yh[i])*eps)*Sqrt(h/(b-a));
					if(er[i]==0)er[i]=tol[i]/4;
					}
				
				// setting up the tolerance ratios
				double errfactor = tol[0]/Abs(er[0]);
				for(int i=1;i<tol.size;i++){
					if(tol[i]/Abs(er[i])<errfactor){
						errfactor=tol[i]/Abs(er[i]);
						}
					}
				
				// Now setting up what to do, if error is lower than tolerance:
				bool factorAccepted = true;
				for(int i=0;i<tol.size;i++){
					if(Abs(er[i])>tol[i]) factorAccepted = false;
				}

				// creating hi+1 = hi * (taui/ei)^power * safety
				double power = 0.25, safety = 0.95;
				double hi1 = h * Pow(errfactor,power)*safety;
				
				if(factorAccepted == true){
					t+=h; yt=yh; h=hi1; 
					if(ts!=null) ts.Add(t);
					if(ys!=null) ys.Add(yt);
					}
				else if(factorAccepted == false){
					// Had to put in error if bad step after assignment roots B.....
					Error.WriteLine($"bad step: x={a}");
					h=hi1;
				}
				}return yt; //return nsteps; return 
		}
	} 
						

				

