using System;
using static System.Console;
using static System.Math;
public partial class root{

	/* I created a updated matrix inverse for this, which can be seen in library/matrix/gramschmidt.cs.
		It works by finding the R decomposition inverse. */


	public static matrix Jacobian(Func<vector,vector> f, vector xstart, vector fx=null, double dx = 1e-6){
		vector x = xstart.copy(); int n=x.size; matrix J = new matrix(n,n);
		
		if(fx==null) fx=f(x);
		
			for(int j = 0; j<n;j++){
				x[j]+=dx;
				vector df=f(x)-fx;
				for(int i=0; i<n; i++){
					J[i,j] = df[i]/dx;
				}
				x[j]-=dx;
			}
		return J;
	}

	
		
	// This is a combination of Jacobian matrix and Newtons method.
	// This is called the Newton-Raphson method

	// finite-difference is linearization from point to point.
	// This is widely used in modelleing, since it's a numerically approximation

	public static vector newton(Func<vector,vector> f, vector xstart, double eps = 1e-4, double dx = 1e-7){
		// Starting with defining the Jacobi matrix
		vector fx;
		vector fy;
		vector x = xstart.copy();
		vector y;
				
		fx = f(x);
		
		while(true){
		
		
			matrix J = Jacobian(f, x,fx);
			matrix Jinv = grams.inverse(J);
			vector Dx = -Jinv * fx; //Stepper
			double s = 1;
			// backtracking linesearch
			while(true){ 
				y=x+Dx*s;
				fy=f(y);
				if(fy.norm()<(1-s/2)*fx.norm()){
					break;
					}
				else if(s<1.0/32){
					break;
					}
				else s/=2;
				}
			
			x = y;
			fx = fy;

			if(fx.norm()<eps){
				Error.Write($" fx.norm() {fx.norm()} is bigger than eps {eps}\n");
				break;
			}
			else if(x.norm()<dx){
			Error.Write($"Step size {Dx.norm()} is below finite difference dx\n");
			
			break;
			}
			}
			
		return x;
		
	}	
}
	
