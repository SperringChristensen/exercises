using System;
using System.IO;
using System.Diagnostics;
using static System.Math;
using static System.Console;
class main{
	static void Main(){
		// Using the integral from last excercise:
		vector a = new vector("0 0 0");
		vector b = new vector($"{PI} {PI} {PI}");
		
		Func<vector,double> m = v => (Pow(1-Cos(v[0])*Cos(v[1])*Cos(v[2]),-1))/PI/PI/PI;
		
		Stopwatch stopwatch = new Stopwatch();
		stopwatch.Start();
		int Nmax = 40000;
		var Bplot = new StreamWriter("Bplot.txt");
		double manal = 1.3932039296856768591842462603255;
		for(int i=1; i<Nmax; i++){
			vector[] result = monte.plainmcs(m,a,b,i);
			vector sum = result[0];
			Bplot.Write($"{i} {1/Sqrt(i)} {Sqrt(manal-sum[0])}\n");
			}
		Bplot.Close();
		stopwatch.Stop();
		WriteLine($"Elapsed time: minutes{stopwatch.Elapsed.Minutes} seconds {stopwatch.Elapsed.Seconds}");
	}
}
		
		
		
