using System;
using System.IO;
using static System.Math;
using static System.Console;
class main{
	static void Main(){
		int n = 5;
			
		Func<vector,double> f = v => Sin(v[0]);
		Func<vector,double> g = v => 2*v[0]*Exp(v[0]*v[0]);
		Func<vector,double> h = v => v[0]*v[0]*v[0];
		vector a = new vector("0");
		vector b = new vector("1");
		int dim = 0;
		int N = 10000000;
		
					
		vector[] result = monte.plainmcs(f, a, b, N);		
		vector[] result1 = monte.plainmcs(g,a,b,N);
		vector[] result2 = monte.plainmcs(h,a,b,N);
		vector sum = result[0];
		vector sum1 = result1[0];
		vector sum2 = result2[0];
		vector err = result[1];
		vector err1 = result1[1];
		vector err2 = result2[1];
		
		WriteLine($"Testing the monte carlo integrator");
		WriteLine($"Sin(x), analytical result = 0.460");
		sum.print("Sin(x), estimate =");
		err.print("Error = ");
		WriteLine($"\n2*x*exp(x^2), analytical result = 1.718");
		sum1.print("2*x*exp(x^2), estimate =");
		err1.print("Error = ");
		WriteLine($"\nx^3, analytical result = 0.250");
		sum2.print("x^3, estimate = ");
		err2.print("Error =");

		Func<vector,double> m = v => (Pow(1-Cos(v[0])*Cos(v[1])*Cos(v[2]),-1))/PI/PI/PI;
		vector a1 = new vector("0 0 0");
		vector b1 = new vector($"{PI} {PI} {PI}");
		
		vector[] mresult = monte.plainmcs(m, a1, b1, N);
		vector msum = mresult[0];
		vector merr = mresult[1];
		double manal = 1.3932039296856768591842462603255;
		WriteLine($"\nThe diffucult singular integral, analytical result = {manal}");
		WriteLine($"The estimated result ={msum[0]}");
		merr.print("Error = ");
		WriteLine($"Diff from estimated and analytical solution = {msum[0]-manal}");
		}
	}
