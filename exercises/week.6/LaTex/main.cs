using System;
using static System.Console;
using static System.Math;
using static cmath;

class main{
	static int Main(){
		//splitting the integrals
		Func<double,double> N1_0 = (t) => 1 *t;
		double xi=0,xf=1,result,result1,result2,result3,result4,result5;
		result=quad.o8av(N1_0,xi,xf);
		
		Func<double,double> N1_1 = (t) => (1+t)*t;
		result1=quad.o8av(N1_1,xi,xf);
		
		Func<double,double> N1_2 = (t) => (2*t+Pow(t,2))*t;
		result2=quad.o8av(N1_2,xi,xf);
		//WriteLine($"{result} {result1} {result2}");
		
		Func<double,double> N2_0 = (t) => 1 * Pow(t,2);
		result3=quad.o8av(N2_0,xi,xf);
		
		Func<double,double> N2_1 = (t) => (1+t)*Pow(t,2);
		result4=quad.o8av(N2_1,xi,xf);
		
		Func<double,double> N2_2 = (t) => (2*t+Pow(t,2))*Pow(t,2);
		result5=quad.o8av(N2_2,xi,xf);
//	WriteLine($"{result} {result1} {result2} {result3} {result4} {result5}");
		
		// set coefficient values
		Matrix2x2 A = new Matrix2x2();
		A[0, 0] = result1; A[0, 1] = result2;
		A[1, 0] = result4; A[1, 1] = result5;
				
		// declare a vector of right-hand side
		Vector2 b = new Vector2();
		
		// set constants on the right-hand side
		b[0] = -result; b[1] = -result3;
		//WriteLine(A[0, 0]);
		// declare a solution vector
		Vector2 x;
		
		//solve the system
		if (Matrix2x2.SolveEquations(out x, A, b) == 0){
			//put down results
		//	WriteLine("x[0] = {0:G}",x[0]);
		//	WriteLine("x[1] = {0:G}",x[1]);
			}
		else{
			WriteLine("Solution not found");
		}
		WriteLine("The solved constant from the system og linear equations");
		WriteLine($"a1 = {-1*x[0]}, a2 = {x[1]}");
			//WriteLine($"exact a1 = 0.83222, exact a2 = 0.2036");
		WriteLine("t, approximated function, exact function");
		for(double t=0;t<=2;t+=0.025)
			WriteLine($"{t} {wrmfunc.aproxx(t)} {wrmfunc.exact(t)}");
		return 0;
		}	
}

