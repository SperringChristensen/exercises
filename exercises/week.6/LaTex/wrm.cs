using System;
using static System.Console;
using static System.Math;
using static cmath;
public static partial class wrmfunc{
		public static double aproxx(double t){
		// the approximated function
		double a1=0.914192154315291,a2=0.2856321568797458;
		return 1-a1*t+a2*Pow(t,2);
		}
		public static double exact(double t){
		//	double a1=0.8272,a2=0.1986;
		//	return 1-a1*t+a2*Pow(t,2);
			return Exp(-t);
		}	
}
