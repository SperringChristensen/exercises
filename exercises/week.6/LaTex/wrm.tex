\documentclass[twocolumn]{article}
\usepackage{graphicx}
\usepackage{float}
%\usepackage{svg}
\title{Weighted Residual Methods}
\author{F.C}
\begin{document}
\maketitle

\section{Introduction to Weighted Residual Methods}
Weighted residual methods (WRM) is a method used to numerically approximate a solution to the  differential equations of the form seen in equation \ref{eq:1}.
\begin{equation}\label{eq:1}
	\mathcal{L}(\Phi) + f(x) = 0
\end{equation}
Here $\mathcal{L}$ is given as \ref{eq:2}.
\begin{equation}\label{eq:2}
	\mathcal{L} = \frac{d² y_a}{dx²}
\end{equation} 
The dependent and unkown value in WRM \ref{eq:1} is the $\Phi(x)$, where $f(x)$ is a known function. 
The WRM involves two major steps, where the first step \textbf{1} is an approximate solution, trial function $\phi$, based upon general behavior of the dependent variable is assumed. 
The trial function is often selected to satisfy the boundary conditions for $\Phi$. 
Since the trial function isn't the "exact" function, but only an approximation to the exact function, it results in an error which is called the residual. 
The residual can be noted as the error of approximation, where the WRM will minimize the residual over the entire solution domain to produce a system of algebraic equations.
The second step \textbf{2}, is to solve the system of equations resulting from the first step, to yield the approximate solution. \cite{1} \newline
When $\Phi(x) \approx  \phi(x)$, it's unlikely that the equation is satisfied, and therefore equation \ref{eq:1} can be rewritten as \ref{eq:3}, or written as \ref{eq:4}.
\begin{equation}\label{eq:3}
	\mathcal{L}(\phi)+f \neq 0
\end{equation}
\begin{equation}\label{eq:4}
	\mathcal{L}(\phi)+f = R
\end{equation}
Here $R$ from equation \ref{eq:4} is residual, which is dependent on the changes in $x$, and therefore $R$ becomes $R(x)$.
Too weight the residual over the whole domain, an arbitrary weight function, $w(x)$ is introduced, and then integrated over the domain $D$ to obtain equation \ref{eq:5}, which is forced to minimize over the whole domain.
\begin{equation}\label{eq:5}
	\int_D w(x)R(x) dD = 0
\end{equation}
\section{Weight Function}
Some of the standard weight functions used in WRM are:
\begin{itemize}
	\item Point Collocation Method
	\item Subdomain Collocation Method
	\subitem Divide the domain into sub domains to increase accuracy
	\item Least Square Method
	\item Galerkin Method
\end{itemize}
Galerkin method is the most common weight function used to solve finite element method problems, and therefore, it will be the one elaborated in this paper.
\subsection{Galerkin Method}
Assuming that the trial solution for equation \ref{eq:5} is as seen in equation \ref{eq:6},
\begin{equation}\label{eq:6}
	\phi(x) = a_0 + a_1*x + a_2*x^2 + ... + a_n*x^n
\end{equation}
it can be rewritten as \ref{eq:7}, where $N_0,N_1,....,N_n$ are $(n+1)$ linearly independent functions of $x$. 
\begin{equation}\label{eq:7}
	\phi(x) = N_0(x) + \sum_{i=1}^n a_i*N_i(x)
\end{equation}
These $N(x)$ functions are called basis functions, interpolation functions, or shape functions. 
The first term $N_0$ is left outside the sum because it is associated with part or all of the ninitial or boundary conditions. \cite{2}
\subsection{Galerkin Method Example}
The exact solution to this example is equation \ref{eq:exact}.
\begin{equation}\label{eq:exact}
	u(x) = \exp(-t)
\end{equation}
For Galerkin method equation \ref{eq:5} can be rewritten as equation \ref{eq:8} for a domain of 0 to 1.
\begin{equation}\label{eq:8}
	\int_0^1R(x)*N_i(x)=0, i=1,2
\end{equation}
Here $N_i(x)$ is chosen as the weighting functions.
In this example, the trial function will be rewritten using basis function, as seen in equation \ref{eq:9}.
\begin{equation}\label{eq:9}
	N_0(x)+a_1*N_1(x)+a_2*N_2(x)^2
\end{equation}
With $N_i$ as: $N_0(x)=1$, $N_1(x)=x$ and $N_2(x)=x^2$. \newline
Thus, plugging the residual equation \ref{eq:10} into equation \ref{eq:8} and using the basis function from equation \ref{eq:9}, yields equation \ref{eq:11} and \ref{eq:12}
\begin{equation}\label{eq:10}
	R(x)=1+a_1*(1+x)+a_2(2*x+x^2)
\end{equation}
\begin{equation}\label{eq:11}
	\int_0^1(1+a_1*(1*x)+a_2*(2*x+x^2))*x dx = 0
\end{equation}
\begin{equation}\label{eq:12}
	\int_0^1(1+a_1*(1*x)+a_2*(2*x+x^2))*x^2 dx = 0
\end{equation}
Then by integrating equation  \ref{eq:11} and \ref{eq:12} and solving by system of linear equations, as seen in equation \ref{eq:13}
\begin{equation}\label{eq:13}
	\left[ {\begin{array}{cc}
		\int_0^1 (1+x)*x dx & \int_0^1 (2*x+x^2)*xdx \\
		\int_0^1 (1+x)*x^2dx & \int_0^1 (2*x+x^2)*x^2dx \\
	\end{array} } \right]
	\left[ {\begin{array}{c}
		a_1 \\
		a_2 \\
	\end{array} } \right]
	= \left[ {\begin{array}{c}
		-\int_0^1 1*x \\
		-\int_0^1 1*x^2\\
	\end{array} } \right]
\end{equation}
, $A.x=b$, by $x=A^1.b$, yields $a_1 = -32/35$ and $a_2 = 2/7$. \newline
This gives the Galerkin approximation seen in equation \ref{eq:14}. 
Figure \ref{fig:1} shows a comparison of the exact soluton with the Galerkin.
\begin{equation}\label{eq:14}
	\phi(x)=1-\frac{32}{35}*x+\frac{2}{7}*x^2
\end{equation}
\begin{figure}[H]\label{fig:1}
	\includegraphics[scale=0.5]{plot.png}
	\caption{Galerkin solution vs. exact solution}
\end{figure}
The approximated Galerkin solution only differ from the exact solution, equation \ref{eq:exact}, after $x=1$.\cite{2}
\newpage

\begin{thebibliography}{99}
\bibitem{1} A.Salih, Weighted Residual Methods, Department of Aerospace Engineering, Indian Institute of Space Science and Technology, Thiruvananthapuram, December 2016
\bibitem{2} https://en.wikiversity.org/wiki/Introduction$\_$to$\_$finite$\_$elements/Weighted$\_$residual$\_$methods
$\#$Minimizing$\_$$\%$7F'$\%$22$\%$60UNIQ--postMath-00000011-QINU$\%$60$\%$22'$\%$7F:$\_$Collocation$\_$Method 
\end{thebibliography}

\end{document}

