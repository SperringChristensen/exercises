using System;
using static System.Console;
using static System.Math;
using static cmath;
class main{
	static int Main(){
		double x=2;
		Write("sqrt(2) = ",x, Math.Sqrt(x));
		Write(Sqrt(x));
		double i=3;
		Write("\n i=3, e^i =");
		Write(exp(i));
		Write("\n e^(i*pi) = ");
		Write(exp(i*Math.PI));
		Write("\n i^i = ");
		Write(Pow(i,i));
		Write("\n sin(i*pi) = ");
		Write(sin(i*Math.PI));
		Write(" Would give 0 with a standard calculator \n");
		complex I = new complex(0,1);
		complex ii = I*I;
		Write("exp(I) = ");
		Write(exp(I));
	return 0;
	}
}

