using System;
using static System.Console;
using static System.Math;
using static cmath;
using static signature;
class main{
	static int Main(){
		int i=1; while(i+1>i) {i++;};
		Write("my max int = {0}\n",i);
		Write("compred with int.MaxValue = {0}\n",int.MaxValue);
		int k=1; while(k-1<k) {k++;};
		Write("my min int = {0}\n",k);
		Write("compared with int.MinValue = {0}\n",int.MinValue);
		double x=1; while(1+x!=1){x/=2;} x*=2;
		float y=1F; while((float)(1F+y) != 1F){y/=2F;} y*=2F;
		Write("double (64)bit = {0}\n",x);
		Write("compared with Math.Pow = \n");
		Write(Math.Pow(2,-52));
		Write("float (with comma) = {0}\n",y);
		Write("compared with Math.Pow = \n");
		Write(Math.Pow(2, -23));
		int max=int.MaxValue/2; // max = maximum integers
		float float_sum_up = 1f;
		for(int j=2;j<max;j++)float_sum_up+=1f/j;
		Write("\n float_sum_up={0}\n",float_sum_up);
		Write("1f = {0}\n",1F);
		Write("1f/max = {0}\n",1f/max);		
		float float_sum_down = 1f/max;
		for(int j=max-1;j>0;j--)float_sum_down+=1f/j;
		Write("float_sum_down = {0}\n",float_sum_down);
		Write("The difference is in the index, where up moves downwards, until max is bigger than i, and the other is until i is bigger than zero.\n");
		Write("I would say that it converge as a function of max. I'm not completely sure why, but it's my best guess.\n");
		for(int j=2;j<max;j++)x+=1f/j;
		Write("for double upwards = {0}\n",x);
		double t=1/max;
		for(int j=max-1;j>0;j--)t+=1f/j;
		Write("for double downwards = {0}\n",t);
		Write("The reason why this sum is higher, is because double is 8 bit and float is 4 bit.\n");
		double a=10;
		double b=2;
		double tau=1e-9;
		double epsilon=1e-9;
		Write("is true = {0}\n",approx(a,b,tau,epsilon)); 
	return 0;
	}
}
