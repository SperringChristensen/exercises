using System;
using static System.Console;
using static System.Math;
public class signature{
public static bool approx(double a, double b, double tau=1e-9, double epsilon=1e-9){
	double d=(a-b),s=a+b;
	if( d<tau ) return true;
	else if ( d/s < tau/2 ) return true;
	else return false;
	}
}
