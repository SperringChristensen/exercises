using System;
using static System.Console;
public static class ivector3dfunc{
	public static double dot(ivector3d u, ivector3d v){
		return u.x*v.x+u.y*v.y+u.z*v.z;
	}
}
