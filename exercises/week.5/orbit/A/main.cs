using System;
using static System.Console;
using static System.Math;
using static cmath;
using System.Collections;
using System.Collections.Generic;
class main{
	static int Main(){
		// Define
		// y0 = u
		// y1 = u'
		// gives the standard form
		// u'=u * (1-u))
		WriteLine("i, x, y, checked value, yerror");
		double xi=0,xf=3,y0=0.5;
		Func<double,vector,vector> f=(x,y)=> new vector(y[0] * (1-y[0]),0);
		vector ya=new vector(y0,0);
		List<double> xs=new List<double>();
		List<vector> ys=new List<vector>();
		ode.rk23(f,xi,ya,xf,xlist:xs,ylist:ys);
		for(int i=0;i<xs.Count;i++){
			double check;
			double yerr;
			check=1/(1+exp(-xs[i]));
			yerr=(ys[i][0]-check)/check;
			WriteLine($"{i} {xs[i]} {ys[i][0]} {check} {yerr}");
		
			}
		return 0;
		}
}

