using System;
using static System.Console;
using static System.Math;
using static cmath;
using System.Linq;
using System.Collections;
using System.Collections.Generic;
class main{
	static int Main(){
		// define
		// y0 = u
		// y1 = u'
		// y2 = u''
		// u''(x) + u(x) = 1+epsilon u(x)² =>
		// u''(x) = 1 + epsilon u(x)² - u(x)
		// rewritten as:
		// y0'=y1
		// y1'=1-y+epsilon*y*y 
		double xi=0,xf=4*PI,y0=1,y1=0;
		double epsilon = 0;
		Func<double,vector,vector> f=(x,y)=> new vector(y[1],1-y[0]+epsilon*y[0]*y[0]);
		vector ya=new vector(y0,y1);
		List<double> xs=new List<double>();
		List<vector> ys=new List<vector>();
		ode.rk23(f,xi,ya,xf,xlist:xs,ylist:ys);
		for(int i=0;i<xs.Count;i++){
			var abe = ($"{i} {xs[i]} {ys[i][0]}");
			WriteLine(abe);	
					}
		
		Func<double,vector,vector> g=(x,y)=> new vector(y[1], 1-y[0]+epsilon*y[0]*y[0]);
		double g0=1,g1=-0.5;
		vector yb = new vector(g0,g1);
		List<double> xs1=new List<double>();
		List<vector> ys1=new List<vector>();
		ode.rk23(g,xi,yb,xf,xlist:xs1,ylist:ys1);
		for(int i=0;i<xs1.Count;i++){
			WriteLine($"{i} {xs1[i]} {ys1[i][0]}");
			}
			


		double epsilon1=0.01;
		Func<double,vector,vector> h=(x,y)=> new vector(y[1], 1-y[0]+epsilon1*y[0]*y[0]);
		List<double> xs2=new List<double>();
		List<vector> ys2=new List<vector>();
		ode.rk23(h,xi,yb,xf,xlist:xs2,ylist:ys2);
		for(int i=0;i<xs2.Count;i++){
			WriteLine($"{i}  {xs2[i]} {ys2[i][0]}");
		}
		
		return 0;
		}
}
