using System;
using static System.Console;
using static System.Math;
using static cmath;
class main{
	static int Main(){
		Func<double,double> f = (x) => Sqrt(x) * exp(-x);
		double a=0,b=double.PositiveInfinity,result;
		result=quad.o8av(f,a,b);
		// Gamma
		WriteLine($" int(sqrt(x)*exp(-x)) a=0,b=infinity = {result}");
		WriteLine("checked = {0}\n", 0.5 * Sqrt(PI));		

		Func<double,double> g = (x) => x/(exp(x) -1);
		double a1=0,b1=double.PositiveInfinity,result1;
		result1=quad.o8av(g,a1,b1);
		// Bernoulli number
		WriteLine($" int x/(exp(x)-1) a=0, b=infinity = {result1}");
		WriteLine("checked = {0}\n", Pow(PI,2) / 6);
		return 0;
		
		}
}

