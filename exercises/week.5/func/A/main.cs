using System;
using static System.Console;
using static System.Math;
using static cmath;
class main{
	static int Main(){
		Func<double,double> f = (x) => Log(x)/Sqrt(x);
		double a=0,b=1,result;
		result=quad.o8av(f,a,b);
		WriteLine($"First integration = {result}\n");
		/* or */
		result=quad.o8av(f,a,b,acc:1e-10,eps:1e-10);
		WriteLine($"Integration based upon accuracy = {result}\n");

		Func<double,double> g = (x) => exp(-Pow(x,2));
		double a1=double.NegativeInfinity,b1=double.PositiveInfinity,result1;
		result1=quad.o8av(g,a1,b1);
		WriteLine($"Integration of exp = {result1}\n");

		for (int p = 0; p < 5; p++){
		Func<double,double> h = (x) => Pow((Log(1/x)),p);
		double a2=0,b2=1,result2;
		result2=quad.o8av(h,a2,b2);
		WriteLine($"Integration with p = {result2}\n");
		}
		return 0;
	}
}
