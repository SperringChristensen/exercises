using System;
using static System.Console;
using static System.Math;
using static cmath;
class main{
	static int Main(){
		WriteLine("alpha, trial function, Hamiltonian-integral, E(alpha)");
		for (double alpha = 0.1; alpha < 3; alpha+=0.1){
			Func<double,double> f = (x) => exp(-alpha*Pow(x,2));
			double a=double.NegativeInfinity,b=double.PositiveInfinity,result;
			result=quad.o8av(f,a,b);
						
			Func<double,double> g = (x) => ((- Pow(alpha,2) * Pow(x,2) / 2) + alpha/2 + Pow(x,2)/2) * exp(-alpha * Pow(x,2));
			double result1;
			result1=quad.o8av(g,a,b);

			double Ealpha;
			Ealpha=result1/result;
			WriteLine("{0} {1} {2} {3}",alpha,result,result1,Ealpha);
			}
			
			return 0;
			
		}
}
