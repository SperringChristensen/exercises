using System;
using static System.Console;
using static System.Math;
using static cmath;
public static partial class specfunc{
	public static double erf(double x){
	// Single precision error function (Abramowits and Stegun)
		double[] a={0.254829592,-0.284496736,1.421413741,-1.453152027,1.061405429};
		double t = 1/(1+0.3275911*x);
		double sum=t*(a[0]+t*(a[1]+t*(a[2]+t*(a[3]+t*a[4]))));/* the right thing */
		return 1-sum*Exp(-x*x);
		}
	public static double gamma(double x){
		// single precision gamma function)
		if(x<0)return Math.PI/sin(Math.PI*x)/gamma(1-x);
		if(x<9)return gamma(x+1)/x;
		double lngamma=x*Log(x+1/(12*x-1/x/10))-x+Log(2*Math.PI/x)/2;
		return Exp(lngamma);
		}
	public static double gammaln(double x){
		if(x<0)return double.NaN;
		if(x<9)return gammaln(x+1)-Log(x);
			double result = (x*Log(x+1/12*x-1/x/10))-x+Log(2*Math.PI/x)/2;
			return result;
		}
}

