using System;
using static System.Console;
using static System.Math;
using static cmath;
class stdin{
	static void Main(){
		string s;;
		while((s = Console.ReadLine()) != null){
			char[] separator = {' '};
			string[] d = s.Split(separator);
			Console.WriteLine("x, sin(x), cos(x)\n");	
			foreach(string t in d){
				int x = Convert.ToInt32(t);
				//Console.WriteLine("x={0}",x);
                	        //Console.WriteLine("sin(x) = {0}",sin(x));
                        	//Console.WriteLine("cos(x) = {0}",cos(x));
				WriteLine("{0} {1} {2}",x,sin(x),cos(x));
			}
			}
		}
}

