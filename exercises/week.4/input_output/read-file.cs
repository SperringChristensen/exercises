using System;
using static System.Console;
using static System.Math;
using static cmath;
class rfile{
	static int Main(string[] args){
		if(args.Length < 2)return 1;
		string infile = args[0];
		string outfile = args[1];
		string[] lines = System.IO.File.ReadAllLines(infile);
		System.IO.StreamWriter outstream = new System.IO.StreamWriter(outfile,append:false);
		outstream.WriteLine("# x sin(x) cos(x)");
		foreach(string line in lines){
		int x = Convert.ToInt32(line);
		outstream.WriteLine("{0} {1} {2}",x,sin(x),cos(x));
		}
		outstream.Close();
		return 0;
		}
}


