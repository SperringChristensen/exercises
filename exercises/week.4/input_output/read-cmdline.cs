using System;
using static System.Console;
using static System.Math;
using static cmath;
class main{
static int Main(string[] args){
	if(args.Length==0) {
		Console.Error.Write("there was no argument\n");
		return 1;
		}
	else {
		Console.WriteLine("x, sin(x), cos(x)\n");
		foreach(var s in args){
			double x=double.Parse(s);
			Console.WriteLine("x={0}",x);
			Console.WriteLine("sin(x) = {0}",sin(x));
			Console.WriteLine("cos(x) = {0}",cos(x));
			}
		return 0;
		}
}
}	
