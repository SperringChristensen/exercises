public class Vector3{
	private double[] v = new double[3];
	public Vector3(){
	v[0] = v[1] = v[2] = 0;
	}
	public double this[int i]{
		get{
			if (i<0 || i>=3)
			return 0;
		else
			return v[i];
		}
		set{
			if(!(i<0 || i>=3))
			v[i]=value;
		}
	}
	public double x{
		get { return v[0];}
		set{v[0]=value;}
	}
	public double y{
		get {return v[1];}
		set{v[1]=value;}
	}
	public double z{
		get {return v[2];}
		set{v[2]=value;}
	}
	public static Vector3 CrossProduct(Vector3 a, Vector3 b){
		Vector3 r = new Vector3();
		r.x= a.y * b.z - a.z * b.y;
		r.y= a.z * b.x - a.x * b.z;
		r.z = a.x * b.y - a.y * b.x;
		return r;
	}
}
